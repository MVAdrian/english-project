-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for laravel

USE `kimobeco_kimobe`;

-- Dumping structure for table laravel.achivements
CREATE TABLE IF NOT EXISTS `achivements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` tinytext,
  `title` tinytext,
  `description` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.achivements: ~3 rows (approximately)
/*!40000 ALTER TABLE `achivements` DISABLE KEYS */;
INSERT INTO `achivements` (`id`, `icon`, `title`, `description`) VALUES
	(1, '/assets/achivements/001.png', 'Learner', 'You achive this by accessing the Lessons page for the first time.'),
	(2, '/assets/achivements/002.png', 'Project Maker', 'You achive this by accessing the Projects page for the first time.'),
	(3, '/assets/achivements/003.png', 'Homework Solver', 'You achive this by finishing you first homework.');
/*!40000 ALTER TABLE `achivements` ENABLE KEYS */;

-- Dumping structure for table laravel.assignments
CREATE TABLE IF NOT EXISTS `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_id` int(11) NOT NULL DEFAULT '0',
  `homework_id` int(11) NOT NULL DEFAULT '0',
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.assignments: ~8 rows (approximately)
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
INSERT INTO `assignments` (`id`, `classroom_id`, `homework_id`, `end_date`, `created_at`, `updated_at`) VALUES
	(1, 1, 7, '2019-11-28 00:00:00', '2019-11-27 18:36:16', '2019-11-27 18:36:16'),
	(2, 2, 7, '2019-11-28 00:00:00', '2019-11-27 18:36:16', '2019-11-27 18:36:16'),
	(3, 3, 7, '2019-11-28 00:00:00', '2019-11-27 18:36:16', '2019-11-27 18:36:16'),
	(4, 4, 8, '2019-11-30 00:00:00', '2019-11-27 20:17:48', '2019-11-27 20:17:48'),
	(5, 1, 8, '2019-12-07 00:00:00', '2019-11-30 11:16:39', '2019-11-30 11:16:39'),
	(6, 2, 8, '2019-12-07 00:00:00', '2019-11-30 11:16:39', '2019-11-30 11:16:39'),
	(7, 3, 8, '2019-12-07 00:00:00', '2019-11-30 11:16:39', '2019-11-30 11:16:39'),
	(8, 4, 10, '2019-12-07 00:00:00', '2019-11-30 11:21:04', '2019-11-30 11:21:04');
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Dumping structure for table laravel.chapters
CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(50) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.chapters: ~2 rows (approximately)
/*!40000 ALTER TABLE `chapters` DISABLE KEYS */;
INSERT INTO `chapters` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
	(1, 'Chapter 1: Learning words', 'In this chapter it is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).', '2019-11-21 12:56:40', '2019-11-21 12:56:40'),
	(2, 'Chapter 2 : Learning phrases', 'What if i told you there are few ways to do Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', '2019-11-21 12:57:36', '2019-11-21 12:57:36'),
	(3, 'Chapter 3 : Learning phrases', 'descriere', '2019-11-30 11:31:47', '2019-11-30 11:31:47');
/*!40000 ALTER TABLE `chapters` ENABLE KEYS */;

-- Dumping structure for table laravel.classrooms
CREATE TABLE IF NOT EXISTS `classrooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom` int(2) DEFAULT NULL,
  `letter` char(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `classroom_letter` (`classroom`,`letter`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.classrooms: ~12 rows (approximately)
/*!40000 ALTER TABLE `classrooms` DISABLE KEYS */;
INSERT INTO `classrooms` (`id`, `classroom`, `letter`) VALUES
	(1, 9, 'A'),
	(2, 9, 'B'),
	(3, 9, 'C'),
	(4, 10, 'A'),
	(5, 10, 'B'),
	(6, 10, 'C'),
	(7, 11, 'A'),
	(8, 11, 'B'),
	(9, 11, 'C'),
	(10, 12, 'A'),
	(11, 12, 'B'),
	(12, 12, 'C');
/*!40000 ALTER TABLE `classrooms` ENABLE KEYS */;

-- Dumping structure for table laravel.completed_assignments
CREATE TABLE IF NOT EXISTS `completed_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `homework_id` int(11) DEFAULT NULL,
  `final` text,
  `json` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_homework_id` (`user_id`,`homework_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.completed_assignments: ~1 rows (approximately)
/*!40000 ALTER TABLE `completed_assignments` DISABLE KEYS */;
INSERT INTO `completed_assignments` (`id`, `user_id`, `homework_id`, `final`, `json`, `created_at`, `updated_at`) VALUES
	(6, 1, 8, '{"grid":100,"text":92.31,"total":96.16}', '{"_token":"mDsnCCMQwY4tz6oq0XZJkJG6rDTGAVt8XkKqn1TD","answer_grid":{"1":"Yes"},"answer_text":{"2":"Adriana"},"homework_id":"8"}', '2019-11-28 22:26:38', '2019-11-28 22:26:38'),
	(7, 1, 10, '{"grid":100,"text":16.67,"total":58.34}', '{"_token":"dt0jBkWZPJQJgXd6LLFv6iJjkf4WIj5MluDCxLLa","answer_grid":{"1":"3"},"answer_text":{"2":"qwerty"},"homework_id":"10"}', '2019-11-30 11:23:10', '2019-11-30 11:23:10');
/*!40000 ALTER TABLE `completed_assignments` ENABLE KEYS */;

-- Dumping structure for table laravel.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table laravel.homeworks
CREATE TABLE IF NOT EXISTS `homeworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext,
  `chapter_id` int(11) DEFAULT NULL,
  `json` text CHARACTER SET utf8 COLLATE utf8_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.homeworks: ~4 rows (approximately)
/*!40000 ALTER TABLE `homeworks` DISABLE KEYS */;
INSERT INTO `homeworks` (`id`, `title`, `chapter_id`, `json`, `created_at`, `updated_at`) VALUES
	(7, 'Test homework Chapter', 1, '{"_token":"7eIIROuJMY6LboznfyC574IX30Wuo0O3sObWZ38A","chapter_id_selected":"1","homework_title":"Test homework Chapter","nr_questions":"","question":{"grid":{"1":"Title here"},"text":{"2":"Hello"}},"answer":{"grid":{"1":{"1":"Answer","2":"Answer","5":"W Answer"}},"text":{"2":{"1":"Dear"}}},"correct_answer":{"grid":{"1":"3"}}}', '2019-11-26 19:26:10', '2019-11-26 19:26:10'),
	(8, 'Homework 5 grid', 1, '{"_token":"RPnxih9TDHRNjWVY6JFAIQV0yexj7EtngFd9iLcu","chapter_id_selected":"1","homework_title":"Homework 5 grid","nr_questions":"","question":{"grid":{"1":"Grid 1 is with 5 answers?"},"text":{"2":"What\'s your name?"}},"answer":{"grid":{"1":{"1":"No","2":"No","3":"No","4":"Yes","5":"No"}},"text":{"2":{"1":"Adrian"}}},"correct_answer":{"grid":{"1":"4"}}}', '2019-11-27 20:17:28', '2019-11-27 20:17:28'),
	(9, 'Title', 2, '{"_token":"dt0jBkWZPJQJgXd6LLFv6iJjkf4WIj5MluDCxLLa","chapter_id_selected":"2","homework_title":"Title","nr_questions":"","question":{"grid":{"1":"ce facem azi?"},"text":{"2":"What\'s your name?"}},"answer":{"grid":{"1":{"1":"1","2":"2","3":"3","4":"4","5":"5"}},"text":{"2":{"1":"Adrian"}}},"correct_answer":{"grid":{"1":"2"}}}', '2019-11-30 11:19:05', '2019-11-30 11:19:05'),
	(10, 'Test homework 1', 1, '{"_token":"dt0jBkWZPJQJgXd6LLFv6iJjkf4WIj5MluDCxLLa","chapter_id_selected":"1","homework_title":"Test homework 1","nr_questions":"","question":{"grid":{"1":"ce facem azi?"},"text":{"2":"What\'s your name?"}},"answer":{"grid":{"1":{"1":"1","2":"2","3":"3","4":"4","5":"5"}},"text":{"2":{"1":"Andrei"}}},"correct_answer":{"grid":{"1":"3"}}}', '2019-11-30 11:19:54', '2019-11-30 11:19:54');
/*!40000 ALTER TABLE `homeworks` ENABLE KEYS */;

-- Dumping structure for table laravel.lessons
CREATE TABLE IF NOT EXISTS `lessons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chapter_id` int(11) NOT NULL DEFAULT '0',
  `name` text NOT NULL,
  `description` text NOT NULL,
  `info` longtext NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.lessons: ~4 rows (approximately)
/*!40000 ALTER TABLE `lessons` DISABLE KEYS */;
INSERT INTO `lessons` (`id`, `chapter_id`, `name`, `description`, `info`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Lesson 1: Learning letters', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. ', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2019-11-21 13:45:05', '2019-11-21 13:45:05'),
	(2, 0, 'Lesson 1: Learning letters', 'What if i told you there are few ways to do Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. \r\n\r\nRichard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.\r\n\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.\r\n\r\nThe first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.\r\n\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2019-11-21 13:58:34', '2019-11-21 13:58:34'),
	(3, 0, 'Lesson 2: Learning letters', 'What if i told you there are few ways to do Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. \r\n\r\nRichard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.\r\n\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.\r\n\r\nThe first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.\r\n\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.\r\n\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2019-11-21 13:59:13', '2019-11-21 13:59:13'),
	(4, 0, 'Lesson 4: Learning letters', 'What if i told you there are few ways to do Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.', 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. <br />\r\n<br />\r\nRichard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.<br />\r\n<br />\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.<br />\r\n<br />\r\nThe first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.<br />\r\n<br />\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.<br />\r\n<br />\r\nLorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2019-11-21 14:01:50', '2019-11-21 14:01:50'),
	(5, 2, 'Lesson 1: Learning letters', 'desc', 'https://drive.google.com/open?id=1SEVEGGdSAzB00tz0-ziaIYK6TRMaX3NMZU7EJuERZ9I', '2019-11-30 11:32:14', '2019-11-30 11:32:14');
/*!40000 ALTER TABLE `lessons` ENABLE KEYS */;

-- Dumping structure for table laravel.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel.migrations: ~0 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table laravel.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel.password_resets: ~1 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('vodafone4mini@gmail.com', '$2y$10$zNvOkcBDoSnBXyZNUWxay.Ur.Z2H9xv369Zg2/5dtgrUhimFROBEG', '2019-11-11 19:55:03');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table laravel.projects
CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `end_date` timestamp NULL DEFAULT NULL,
  `title` tinytext,
  `chapter_id` int(11) DEFAULT NULL,
  `classroom_id` int(11) DEFAULT NULL,
  `json` text CHARACTER SET utf8 COLLATE utf8_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table laravel.projects: ~5 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `end_date`, `title`, `chapter_id`, `classroom_id`, `json`, `created_at`, `updated_at`) VALUES
	(1, '2019-11-30 00:00:00', 'Project test title 1', 1, 1, '{"_token":"8z2WPhpjWKKyVFdTDzkoWujTU8q6aljpq4254DEO","classroom_list":"[1,2","last_day":"2019-11-30","project_title":"Project Title","project_description":"Project Descriptionnnn"}', '2019-11-19 17:56:20', '2019-11-19 17:56:20'),
	(8, '2019-11-30 00:00:00', NULL, 2, 2, '{"_token":"8z2WPhpjWKKyVFdTDzkoWujTU8q6aljpq4254DEO","classroom_list":"[1,2","last_day":"2019-11-30","project_title":"Project Title","project_description":"Project Descriptionnnn"}', '2019-11-19 17:56:20', '2019-11-19 17:56:20'),
	(9, '2019-11-30 00:00:00', NULL, 1, 4, '{"_token":"l6avEVucXK64sIfMePXVIeKeQvPNY4dVLSOnVkZN","classroom_list":"[4","last_day":"2019-11-30","project_title":"Project Title","project_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}', '2019-11-29 16:31:12', '2019-11-29 16:31:12'),
	(10, '2019-12-07 00:00:00', NULL, NULL, 4, '{"_token":"dt0jBkWZPJQJgXd6LLFv6iJjkf4WIj5MluDCxLLa","classroom_list":"[4","last_day":"2019-12-07","project_title":"Project Title","project_description":"asdfhyyu\\r\\nkimio\\r\\nol,o\\r\\n"}', '2019-11-30 11:25:15', '2019-11-30 11:25:15'),
	(11, '2019-12-01 00:00:00', NULL, NULL, 4, '{"_token":"dt0jBkWZPJQJgXd6LLFv6iJjkf4WIj5MluDCxLLa","classroom_list":"[4","last_day":"2019-12-01","project_title":"Project Title","project_description":"hello"}', '2019-11-30 11:26:14', '2019-11-30 11:26:14');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;

-- Dumping structure for table laravel.uploaded_projects
CREATE TABLE IF NOT EXISTS `uploaded_projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `file_name` text,
  `path` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table laravel.uploaded_projects: ~0 rows (approximately)
/*!40000 ALTER TABLE `uploaded_projects` DISABLE KEYS */;
INSERT INTO `uploaded_projects` (`id`, `project_id`, `user_id`, `file_name`, `path`, `created_at`, `updated_at`) VALUES
	(1, 9, 1, 'SquirrelSetup.log', 'C:\\Users\\vodaf\\Desktop\\english-project\\storage/uploads/SquirrelSetup.log', '2019-11-29 19:14:47', '2019-11-29 19:14:47');
/*!40000 ALTER TABLE `uploaded_projects` ENABLE KEYS */;

-- Dumping structure for table laravel.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `classroom_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel.users: ~0 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `classroom_id`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Adrian-Victor Moșoiu', 'vodafone4mini@gmail.com', '4', NULL, '$2y$10$7OcbbvXrveTVWHGX/s2GIe2SRFpD6VlqXA784n753frAvKYPbdBxm', 'ydVnZCqrNN1YyCZHXzdwhEc16SKXfPGX9RKYd9BWumPNtYdSRPdEZOO3shyx', '2019-10-01 19:57:14', '2019-11-30 11:36:45');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
