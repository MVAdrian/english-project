@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ml-0">
        <div class="w-100 card">
            <h2 class="card-header">Assignments</h2>
            <div class="card-body">
                @foreach($homeworks as $homework)
                    <div class="mb-3" style="border-bottom: 1px solid gray">
                        <h3>{{$homework['homework_title']}}</h3>
                        <h4>Until : {{$homework['last_day']}}</h4>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
