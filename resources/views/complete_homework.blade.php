@extends('layouts.app')

@section('content')
{{--    @php(dd($homework))--}}
<div class="col-md-12 align-items-center justify-content-center">
    <h2 class="text-center font-weight-bold">{{$homework['homework_title']}}</h2>
</div>
<div>
    <form method="POST" action="{{ route('correct_homework') }}">
        @csrf
        <div class="w-100 d-flex flex-column justify-content-center align-items-center">
            <div class="my-3 p-3" style="width:80%;background-color:white;border-radius: 10px">
                @if(isset($homework['question']['grid']))
                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Grid Section : Select the correct answer by clicking it. There are no question with multiple choices.</h4>
                    @foreach($homework['question']['grid'] as $question_key => $question)
                        <h4 class="mb-4">{{$question}}</h4>
                        <div class="d-flex flex-column">
                            @foreach($homework['answer']['grid'][$question_key] as $key2 => $answer)
                                <div class="mr-3 mb-3 d-flex align-items-center">
                                    <input class="align-items-center form-control" style="width:5%;font-size: 5px" name="answer_grid[{{$question_key}}]" required type="radio" id="question_grid{{$question_key}}"
                                    value="{{$answer}}">
                                    <label class="mb-0 ml-2" for="answer_grid[{{$question_key}}]" style="font-size: 18px;">{{$answer}}</label>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                @endif
                @if(isset($homework['question']['text']))
                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Text Section : Write your answer in the box below every question.</h4>
                @foreach($homework['question']['text'] as $question_key => $question)
                        <h4 class="mb-4">{{$question}}</h4>
                        <div class="d-flex align-items-center">
                            @foreach($homework['answer']['text'][$question_key] as $answer)
                                <div class="mr-3 w-100">
                                    <input type="text" class="form-control" style="font-size: 1rem" name="answer_text[{{$question_key}}]" required>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                @endif
                @if(isset($homework['essay']))
                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Essay Section : Write your essay in the box below every title.</h4>
                    @foreach($homework['essay'] as $essay_key => $essay)
                        <h4 class="mb-4">{{$essay}}</h4>
                        <textarea class="form-control" rows="5" id="answer_essay[{{$essay_key}}]" name="answer_essay[{{$essay_key}}]" required></textarea>
                    @endforeach
                @endif
                @if(isset($homework['variants']))
                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Variants Section : Select the right answers from the text below.</h4>
                    @foreach($homework['variants'] as $variants_key => $variants)
                        <div class="row ml-0 align-items-center mx-2 py-2" style="border-top: 1px solid gray;border-bottom: 1px solid gray;">
                            @foreach($variants as $item_key => $item)
                                @if(is_string($item))
                                    <p class="mr-2 mb-1" style="font-size: 20px;"> {{$item}} </p>
                                @else
                                    <div class="row ml-0 mr-2 mb-1">
                                        <div class="d-flex align-items-center mr-1 bg-dark text-white p-1" style="border-radius: 10px;">
                                            <input class="align-items-center"  type="radio" name="answer_variants[{{$variants_key}}][{{$item_key}}]" required value="{{$item[0]}}">
                                            <label class="mb-0 ml-1" style="font-size: 20px;">{{$item[0]}}</label>
                                        </div>
                                        <div class="d-flex align-items-center mr-1 bg-dark text-white p-1" style="border-radius: 10px;">
                                            <input class="align-items-center"  type="radio" name="answer_variants[{{$variants_key}}][{{$item_key}}]" required value="{{$item[1]}}">
                                            <label class="mb-0 ml-1" style="font-size: 20px;">{{$item[1]}}</label>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                @endif
                <input type="hidden" value="{{$id}}" name="homework_id">
                <button class="btn btn-primary mt-3 form-control" type="submit">Send Homework</button>
            </div>
        </div>
    </form>
</div>
@endsection
