@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-primary text-white">Lessons</h5>
                    <div class="card-body">
                        @if(isset($lessons))
                            @foreach($lessons as $lesson)
                                <div class="mb-5">
                                    <h6 class="font-weight-bold mb-2">{{$lesson['name']}}</h6>
                                    <p>{{$lesson['description']}}</p>
                                    <a class="btn btn-primary" href="/lesson/{{$lesson['id']}}">Click to learn more about this lesson</a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
