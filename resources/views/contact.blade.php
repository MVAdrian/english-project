@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-primary text-white">Contact</h5>
                    <div class="card-body">
                        <form method="POST" action="/contact_send">
                            @csrf
                            <div class="row m-0 mb-3">
                                <div class="col-md-6">
                                    <label for="first_name">* First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name">* Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" required>
                                </div>
                            </div>
                            <div class="row m-0 mb-3">
                                <div class="col-md-6">
                                    <label for="email">* Email</label>
                                    <input type="email" class="form-control" name="email" id="email" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="phone_number">Phone Number</label>
                                    <input type="text" class="form-control" name="phone_number" id="phone_number">
                                </div>
                            </div>
                            <div class="col-md-12 mb-3">
                                <label for="observation">Observations</label>
                                <textarea name="observations" class="form-control" rows="5"></textarea>
                            </div>
                            <p class="text-muted ml-0">* required field</p>
                            <button class="form-control btn btn-primary" type="submit">Send Observation</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
