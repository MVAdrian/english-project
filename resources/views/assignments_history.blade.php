@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h6 class="mb-2 text-danger"><strong>* Can not review assignments before end date</strong></h6>
                <table class="table table-striped" style="border-top-left-radius: 13px;border-top: none;">
                    <thead class="thead-dark" style="border-top-left-radius: 13px;border-top: none;">
                    <tr>
                        <th scope="col" style="border-top-left-radius: 13px;border-top: none;">Name</th>
                        <th scope="col">Chapter</th>
                        <th scope="col">Due date</th>
                        <th scope="col">Status</th>
                        <th scope="col" style="border-top-right-radius: 13px;border-top: none;">Review</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($all_assignments as $key => $homework)
                            <tr>
                                <td>{{$homework->title}}</td>
                                <td>{{$homework->name}}</td>
                                <td>{{date('Y-m-d',strtotime($homework->end_date))}}</td>
                                <td>
                                    @if($homework->user_id == null)
                                        @if(date($homework->end_date) < date('Y-m-d'))
                                            <button class="btn btn-danger disabled" disabled style="min-width: 138px;">Time expired !</button>
                                        @else
                                            <button class="btn btn-warning disabled" disabled style="min-width: 138px;">Please complete !</button>
                                        @endif
                                    @else
                                        <button class="btn btn-success disabled" disabled style="min-width: 138px;">Completed !</button>
                                    @endif
                                </td>
                                <td>
                                    @if($homework->user_id == null)
                                        @if(date($homework->end_date) < date('Y-m-d'))
                                            <button type="button" class="btn btn-dark disabled" disabled style="min-width: 141px;">Nothing to review</button>
                                        @else
                                            <a href="/complete_homework?homework_id={{$homework->id}}" type="button" class="btn btn-primary" style="min-width: 141px;">Start Homework</a>
                                        @endif
                                    @else
                                        <a href="/complete_homework?homework_id={{$homework->id}}" type="button" class="btn btn-dark" @if(date($homework->end_date) > date('Y-m-d')) disabled @endif style="min-width: 141px;">Review</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
