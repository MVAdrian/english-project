@extends('layouts.app')

@section('content')
<div class="container">
    <div class='row'>
        <div class="col-md-12">
            <div class="card">
                    <h5 class="card-header font-weight-bold bg-primary text-white">Achivements</h5>
                <div class="card-body">
                    @foreach ($achivements as $achivement)
                        <div class="row align-items-center">
                            <img src="{{$achivement['icon']}}" alt="{{$achivement['title']}}" class="achivement-page-item">
                            <div class="my-3 ml-3">
                                <h4 class="font-weight bold">
                                    {{$achivement['title']}}
                                </h4>
                                <p>
                                    {{$achivement['description']}}
                                </p>
                            </div>
                        </div>
                        <div class="my-3 divider"></div>
                    @endforeach 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection