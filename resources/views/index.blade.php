<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Project') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="intro-show">
            <div class="logo-hold">
                <img src="/assets/images/white_logo_transparent_background.png" class="intro-logo">
            </div>
            <p class="logo-title">Mrs. E’s English Class</p>
            <div class="row mx-0 align-items-center justify-content-center w-100">
                <div class="col-md-6 offset-md-3 offset-md-right-3">
                    <a class="btn mt-2 font-weight-bold intro-start mr-3 w-100" href="/login">Get Started</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
