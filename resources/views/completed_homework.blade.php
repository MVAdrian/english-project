@extends('layouts.app')

@section('content')
{{--    @php(dd($homework,$answers))--}}
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header text-white bg-primary text-center">Homework completed !</h5>
                    <div class="card-body">
                        <div class="text-center">
                            @if(isset($homework['question']['grid']))
                            <h4 class="font-weight-bold">Grid percent: {{$final['grid']}}%</h4>
                            @endif
                            @if(isset($homework['question']['text']))
                                <h4 class="font-weight-bold">Text percent: {{$final['text']}}%</h4>
                            @endif
                            @if(isset($homework['variants']))
                            <h4 class="font-weight-bold">Variants percent: {{$final['variants']}}%</h4>
                            @endif
                            @if(isset($homework['essay']))
                                <h4 class="font-weight-bold">Essay percent: {{$final['essay']}}%</h4>
                            @endif
                            <h4 class="font-weight-bold">Final percent: {{$final['total']}}%</h4>
                        </div>
                        <div class="w-100 d-flex flex-column justify-content-center align-items-center">
                            <div class="my-3 p-3" style="width:80%;background-color:white;border-radius: 10px">
                                @if(isset($homework['question']['grid']))
                                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Grid Section : Select the correct answer by clicking it. There are no question with multiple choices.</h4>
                                    @foreach($homework['question']['grid'] as $question_key => $question)
                                        <h4 class="mb-4">{{$question}}</h4>
                                        <div class="d-flex flex-column">
                                            @foreach($homework['answer']['grid'][$question_key] as $key2 => $answer)
                                                <div class="mr-3 mb-3 d-flex align-items-center {{($key2 == $homework['correct_answer']['grid'][$question_key])?'bg-right text-white':''}} {{($answer == $answers['grid_answers'][$question_key])?'bg-wrong text-white':''}}">
                                                    <input disabled class="align-items-center form-control" style="width:5%;font-size: 5px" name="answer_grid[{{$question_key}}]" type="radio" id="question_grid{{$question_key}}"
                                                           value="{{$answer}}" @if($key2 == $homework['correct_answer']['grid'][$question_key]) checked @endif>
                                                    <label class="mb-0 ml-2" for="answer_grid[{{$question_key}}]" style="font-size: 18px;">{{$answer}}</label>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                                @if(isset($homework['question']['text']))
                                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Text Section : Write your answer in the box below every question.</h4>
                                    @foreach($homework['question']['text'] as $question_key => $question)
                                        <h4 class="mb-4">{{$question}}</h4>
                                        <div class="d-flex align-items-center">
                                            @foreach($homework['answer']['text'][$question_key] as $answer)
                                                <div class="mr-3 w-100">
                                                    <div class="row ml-0 w-100">
                                                        <h5 class="font-weight-bold">Correct Answer :</h5>
                                                        <input type="text" class="form-control" style="font-size: 1rem" name="answer_text[{{$question_key}}]" value="{{$answer}}" disabled>
                                                    </div>
                                                    <div class="row ml-0 w-100 mt-2">
                                                        <h5 class="font-weight-bold">Your Answer :</h5>
                                                        <input type="text" class="form-control" style="font-size: 1rem" name="answer_text[{{$question_key}}]" value="{{$answers['text_answers'][$question_key]}}" disabled>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
                                @if(isset($homework['variants']))
                                    <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Variants Section : Select the right answers from the text below.</h4>
                                    @foreach($homework['variants'] as $variants_key => $variants)
                                        <div class="row ml-0 align-items-center mx-2 py-2" style="border-top: 1px solid gray;border-bottom: 1px solid gray;">
                                            @foreach($variants as $item_key => $item)
                                                @if(is_string($item))
                                                    <p class="mr-2 mb-1" style="font-size: 20px;"> {{$item}} </p>
                                                @else
                                                    <div class="row ml-0 mr-2 mb-1">
                                                        <div class="d-flex align-items-center mr-1 bg-dark text-white p-1" style="border-radius: 10px;background-color: {{($item[0] == $homework['variants'][$variants_key][$item_key][$homework['variants'][$variants_key][$item_key][2] - 1])?"#38c172":"#343a40"}}!important; @if($answers['variants_answers'][$variants_key][$item_key] == $item[0] && ($answers['variants_answers'][$variants_key][$item_key] != $homework['variants'][$variants_key][$item_key][$homework['variants'][$variants_key][$item_key][2] - 1])) background-color:darkred!important @endif">
                                                            <input class="align-items-center" disabled  type="radio" value="{{$item[0]}}" @if($answers['variants_answers'][$variants_key][$item_key] == $item[0]) checked @endif >
                                                            <label class="mb-0 ml-1" style="font-size: 20px;">{{$item[0]}}</label>
                                                        </div>
                                                        <div class="d-flex align-items-center mr-1 bg-dark text-white p-1" style="border-radius: 10px;background-color: {{($item[1] == $homework['variants'][$variants_key][$item_key][$homework['variants'][$variants_key][$item_key][2] - 1])?"#38c172":"#343a40"}}!important; @if($answers['variants_answers'][$variants_key][$item_key] == $item[1] && ($answers['variants_answers'][$variants_key][$item_key] != $homework['variants'][$variants_key][$item_key][$homework['variants'][$variants_key][$item_key][2] - 1])) background-color:darkred!important @endif">
                                                            <input class="align-items-center" disabled  type="radio" value="{{$item[1]}}" @if($answers['variants_answers'][$variants_key][$item_key] == $item[1]) checked @endif >
                                                            <label class="mb-0 ml-1" style="font-size: 20px;">{{$item[1]}}</label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    @endforeach
                                @endif
{{--                                @php(dd($homework,$answers))--}}
                                @if(isset($homework['essay']))
                                        <h4 class="my-2 bg-primary p-2" style="border-bottom-left-radius: 10px;border-bottom-right-radius: 10px;color:white">Essay Section</h4>
                                    @foreach($homework['essay'] as $essay_key => $essay_title)
                                            <div class="row ml-0 align-items-center mx-2 py-2" style="border-top: 1px solid gray;border-bottom: 1px solid gray;">
                                                <h5>{{$essay_title}}</h5>
                                                <div>
                                                    @if(isset($answers['essay_answers'][$essay_key]))
                                                        <?php
                                                        echo nl2br($answers['essay_answers'][$essay_key]);
                                                        ?>
                                                    @else
                                                        <p>Answer not found</p>
                                                    @endif
                                                </div>
                                            </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        @if(Auth::user()->is_admin)
                            <div class="col-md-12 d-flex flex-column justify-content-center align-items-center">
                                <form method="POST" action="{{ route('update_grades',['ca_id' => $answer_id]) }}">
                                    @csrf
                                    <div class="row">
                                        @if(isset($homework['question']['grid']))
                                            <div class="mr-2 mb-2">
                                                <label for="grid">Grid Percent</label>
                                                <input class="form-control" type="number" id="grid" name="grid" value="{{$final['grid']}}" required/>
                                            </div>
                                        @endif
                                        @if(isset($homework['question']['text']))
                                            <div class="mr-2 mb-2">
                                                <label for="text">Text Percent</label>
                                                <input class="form-control" type="number" id="text" name="text" value="{{$final['text']}}" required/>
                                            </div>
                                        @endif
                                        @if(isset($homework['variants']))
                                            <div class="mr-2 mb-2">
                                                <label for="variants">Variants Percent</label>
                                                <input class="form-control" type="number" id="variants" name="variants" value="{{$final['variants']}}" required/>
                                            </div>
                                        @endif
                                        @if(isset($homework['essay']))
                                            <div class="mr-2 mb-2">
                                                <label for="essay">Essay Percent</label>
                                                <input class="form-control" type="number" id="essay" name="essay" value="{{$final['essay']}}" required/>
                                            </div>
                                        @endif
                                    </div>
                                    <button class="btn btn-primary mt-3" type="submit">Update grades</button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
