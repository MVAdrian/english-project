@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="card-header bg-primary text-white">Create Chapter</h4>
                    <div class="card-body">
                        <form method="POST" action="{{route('assign_chapter')}}">
                            @csrf
                            <div>
                                <label for="chapter_name">Insert chapter's name</label>
                                <input class="form-control" id="chapter_name" name="chapter_name" placeholder="Name" required>
                            </div>
                            <div class="mt-3">
                                <label for="chapter_name">Insert chapter's description</label>
                                <textarea class="form-control" rows="4"  id="chapter_description" name="chapter_description" placeholder="Description" required></textarea>
                            </div>
                            <button class="form-control btn btn-primary mt-3" type="submit">Assign Chapter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
