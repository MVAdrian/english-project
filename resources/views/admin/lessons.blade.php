@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">Admin Panel - Lessons</h5>
                    <div class="card-body">
                        <div class="row ml-0 align-items-center">
                            <div class="dropdown mb-2">
                                <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Chapters
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($chapters as $key => $chapter)
                                        <a class="dropdown-item" href="/lessons_admin?chapter_id={{$chapter['id']}}">
                                            {{$chapter['name']}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                            <a class="btn btn-secondary ml-2 mb-2" href="/lessons_admin">Clear Filter</a>
                            <a class="btn btn-primary ml-auto mr-2 mb-2" href="/create_lesson">Create</a>
                        </div>
                        <script>

                            function ConfirmDelete()
                            {
                                var x = confirm("Are you sure you want to delete?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }

                        </script>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Chapter</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($lessons as $lesson)
                                <tr>
                                    <td>{{$lesson['name']}}</td>
                                    <td>{{$lesson['chapter_name']}}</td>
                                    <td class="row w-100 mx-0">
                                        <a class="btn btn-primary mr-2 mb-2" href="/lessons_admin/{{$lesson['id']}}">Edit</a>
                                        <form method="POST" action="/lessons_admin/delete/{{$lesson['id']}}" onsubmit="return ConfirmDelete()">
                                            @csrf
                                            <button class="btn btn-danger mb-2" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
