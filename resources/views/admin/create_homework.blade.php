@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-primary text-white font-weight-bold">
                        Create Homework
                    </h5>
                    <div class="card-body">
                        <form id="homework" method="POST" action="{{route('assign_homework')}}">
                            @csrf
                            <div class="form-group row align-items-center" style="margin-left:0px;">
                                <label for="chapter" class="mb-0">Select chapter:</label>
                                <input type="text" id="chapter" style="display:none;">
                                <div class="dropdown ml-3">
                                    <button id="chapter_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Chapters
                                    </button>
                                    <script>
                                        function add_chapter(id,name){
                                            document.getElementById('chapter_selected').innerHTML = 'Chapter selected : '+ name;
                                            document.getElementById('chapter_id_selected').value = id;
                                        }
                                    </script>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($chapters as $key => $chapter)
                                            <button type="button" class="dropdown-item" onclick="add_chapter({{$chapter['id']}},'{{$chapter['name']}}')">
                                                {{$chapter['name']}}
                                            </button>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="height:30px;margin-left:0px;">
                                <p class="text-muted">Chapter selected :&nbsp;</p>
                                <p id="chapter_selected" class="font-weight-bold"></p>
                                <input id="chapter_id_selected" name="chapter_id_selected" style="display:none;">
                            </div>
                            <div class="form-group">
                                <label for="homework_title">Homework Title</label>
                                <input type="text" class="form-control" id="homework_title" name="homework_title" placeholder="Title">
                            </div>
                            <div class="form-group row align-items-center" style="">
                                <input type="text" id="nr_questions" name="nr_questions" style="display:none;">
                                <div class="w-100 px-3">
                                    <script>
                                        function IGQ(){
                                            var length = document.getElementById('question_holder').children.length;
                                            var clone = document.getElementById('question_grid_group');
                                            var div = clone.cloneNode(true);
                                            div.children[0].innerHTML = "Question " + (1 + length);
                                            div.children[1].children[0].name = "question[grid][" + (1 + length) +"]";
                                            div.children[1].children[1].children[1].name = "answer[grid][" + (1 + length) +"][1]";
                                            div.children[1].children[2].children[1].name = "answer[grid][" + (1 + length) +"][2]";
                                            div.children[1].children[3].children[1].name = "answer[grid][" + (1 + length) +"][3]";
                                            div.children[1].children[4].children[1].name = "answer[grid][" + (1 + length) +"][4]";
                                            div.children[1].children[5].children[1].name = "answer[grid][" + (1 + length) +"][5]";
                                            div.children[1].children[6].children[1].name = "correct_answer[grid][" + (1 + length) +"]";
                                            div.classList.remove('hide');
                                            document.getElementById('send_homework').classList.remove('hide');

                                            document.getElementById('question_holder').appendChild(div);
                                        }

                                        function ITQ(){
                                            var length = document.getElementById('question_holder').children.length;
                                            var clone = document.getElementById('question_text_group');
                                            var div = clone.cloneNode(true);
                                            div.children[0].innerHTML = "Question " + (1 + length);
                                            div.children[1].children[0].name = "question[text][" + (1 + length) +"]";
                                            div.children[1].children[1].children[1].name = "answer[text][" + (1 + length) +"][1]";
                                            div.classList.remove('hide');
                                            document.getElementById('send_homework').classList.remove('hide');

                                            document.getElementById('question_holder').appendChild(div);
                                        }

                                        function IEQ(){
                                            var length = document.getElementById('question_holder').children.length;
                                            var clone = document.getElementById('question_essay_group');
                                            var div = clone.cloneNode(true);
                                            div.children[0].innerHTML = "Question " + (1 + length) + ' (essay)';
                                            div.children[1].children[0].name = "essay[" + (1 + length) +"]";
                                            div.classList.remove('hide');
                                            document.getElementById('send_homework').classList.remove('hide');

                                            document.getElementById('question_holder').appendChild(div);
                                        }

                                        function IMQ(){
                                            var length = document.getElementById('question_holder').children.length;
                                            var clone = document.getElementById('question_multi-group_group');
                                            var div = clone.cloneNode(true);
                                            var new_length = (1 + length);
                                            div.children[0].innerHTML = "Question " + new_length;
                                            div.id = 'question_multi-group_group' + new_length;
                                            div.children[1].children[1].children[0].setAttribute('onclick','add_text("'+ div.id + '",' + new_length +')');
                                            div.children[1].children[1].children[0].addEventListener("click", function(event){
                                                event.preventDefault()
                                            });
                                            div.children[1].children[1].children[1].setAttribute('onclick','add_variants("'+ div.id + '",' + new_length +')');
                                            div.children[1].children[1].children[1].addEventListener("click", function(event){
                                                event.preventDefault()
                                            });
                                            div.classList.remove('hide');
                                            document.getElementById('send_homework').classList.remove('hide');
                                            document.getElementById('question_holder').appendChild(div);
                                        }

                                        function add_text(q_id,input_length){
                                            var text_block = document.getElementById('textarea_clone');
                                            var textarea = text_block.cloneNode(true);
                                            var block = document.getElementById(q_id);
                                            var block_length = block.children[1].children[2].childElementCount;
                                            textarea.id = 'textarea_'+ q_id + input_length;
                                            textarea.name = 'variants[' + input_length + ']' + '[' + block_length + ']';
                                            textarea.classList.remove('hide');
                                            block.children[1].children[2].appendChild(textarea);
                                            console.log(q_id);
                                        }

                                        function add_variants(q_id,input_length){
                                            let variants_block = document.getElementById('variants_clone');
                                            let variants = variants_block.cloneNode(true);
                                            let block = document.getElementById(q_id);
                                            let block_length = block.children[1].children[2].childElementCount;
                                            variants.id = 'textarea_'+ q_id + input_length;
                                            variants.children[0].name = 'variants[' + input_length + ']' + '[' + block_length + '][0]';
                                            variants.children[1].name = 'variants[' + input_length + ']' + '[' + block_length + '][1]';
                                            variants.children[2].name = 'variants[' + input_length + ']' + '[' + block_length + '][2]';
                                            variants.classList.remove('hide');
                                            block.children[1].children[2].appendChild(variants);
                                            console.log(q_id);
                                        }
                                    </script>
                                    <button id="grid_button" class="btn btn-primary" type="button" onclick="IGQ()">
                                        Insert grid question +
                                    </button>
                                    <button id="text_button" class="btn btn-primary" type="button" onclick="ITQ()">
                                        Insert text question +
                                    </button>
                                    <button id="essay_button" class="btn btn-primary" type="button" onclick="IEQ()">
                                        Insert essay +
                                    </button>
                                    <button id="multi-grid_button" class="btn btn-primary" type="button" onclick="IMQ()">
                                        Insert Multiple grid question +
                                    </button>
                                    <div id="question_holder" class="w-100">

                                    </div>
                                        <button type="submit" class="btn btn-primary hide" id="send_homework">Assign Homework</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div id="question_grid_group" class="hide card my-3 font-weight-bold question-holder">
                        <h5 id="question_title" class="card-header bg-primary question-holder-header"></h5>
                        <div class="row ml-0 card-body w-100">
                            <input type="text" class="form-control" placeholder="Questions's title here">
                            <div class="form-group mr-3 mt-3">
                                <label for="answer1">Answer 1</label>
                                <input type="text" class="form-control" placeholder="Answer here">
                            </div>
                            <div class="form-group mr-3 mt-3">
                                <label for="answer2">Answer 2</label>
                                <input type="text" class="form-control" placeholder="Answer here">
                            </div>
                            <div class="form-group mr-3 mt-3 deactivated" onclick="this.children[1].disabled=false;this.classList.remove('deactivated')">
                                <label for="answer3">Answer 3</label>
                                <input type="text" class="form-control" placeholder="Answer here" disabled >
                            </div>
                            <div class="form-group mr-3 mt-3 deactivated" onclick="this.children[1].disabled=false;this.classList.remove('deactivated')">
                                <label for="answer4">Answer 4</label>
                                <input type="text" class="form-control" placeholder="Answer here" disabled>
                            </div>
                            <div class="form-group mr-3 mt-3 deactivated" onclick="this.children[1].disabled=false;this.classList.remove('deactivated')">
                                <label for="answer5">Answer 5</label>
                                <input type="text" class="form-control" placeholder="Answer here" disabled>
                            </div>
                            <div class="form-group mt-3">
                                <label for="answer4">Correct Answer</label>
                                <input type="text" class="form-control" placeholder="Correct answer here">
                            </div>
                        </div>
                    </div>
                    <div id="question_text_group" class="hide card mt-3 font-weight-bold question-holder">
                        <h5 id="question_title" class="card-header bg-primary question-holder-header"></h5>
                        <div class="row ml-0 card-body w-100">
                            <input type="text" class="form-control"  placeholder="Questions's title here">
                            <div class="form-group mt-3 w-100">
                                <label>Answer</label>
                                <input type="text" class="form-control" placeholder="Correct answer here">
                            </div>
                        </div>
                    </div>
                    <div id="question_essay_group" class="hide card my-3 font-weight-bold question-holder">
                        <h5 id="question_title" class="card-header bg-primary question-holder-header"></h5>
                        <div class="row ml-0 card-body w-100">
                            <input type="text" class="form-control" placeholder="Essay's title here">
                        </div>
                    </div>
                    <div id="question_multi-group_group" class="hide card my-3 font-weight-bold question-holder">
                        <h5 id="question_title" class="card-header bg-primary question-holder-header"></h5>
                        <div class="row ml-0 card-body w-100">
                            <input type="text" class="form-control" placeholder="Questions's title here">
                            <div class="row mx-0">
                                <button class="btn btn-primary mr-3 mt-3" onclick="add_text()">Add Text</button>
                                <button class="btn btn-primary mr-3 mt-3" onclick="add_variants()">Add Variants</button>
                            </div>
                            <div class="form-group w-100">

                            </div>
                        </div>
                    </div>
                    <textarea id="textarea_clone" class="form-control hide mt-3" rows="1" placeholder="Text here"></textarea>
                    <div id="variants_clone" class="row mx-0 mt-3 hide">
                        <input type="text" class="w-25 mr-2 form-control" placeholder="First variant">
                        <input type="text" class="w-25 mr-2 form-control" placeholder="Second variant">
                        <input type="text" class="w-25 mr-2 form-control" placeholder="Correct varriant">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
