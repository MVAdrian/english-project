@extends ('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary text-white font-weight-bold">
                    Create Project
                </h5>
                <div class="card-body">
                    <form id="homework" method="POST" action="{{route('assign_project')}}">
                        @csrf
                        <div class="form-group row align-items-center" style="margin-left:0px;">
                            <label for="classroom" class="mb-0">Select Classrooms:</label>
                            <input type="text" id="classroom" style="display:none;">
                            <div class="dropdown ml-3">
                                <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Classrooms
                                </button>
                                <script>
                                    function add_classroom(id,classroom,letter){
                                        var text = document.getElementById('classroom_selected').innerHTML;
                                        text = text + classroom +'-'+ letter + " ; ";
                                        document.getElementById('classroom_selected').innerHTML = text;

                                        var classroom_list = document.getElementById('classroom_list').value;
                                        if(classroom_list == "")
                                            classroom_list = "[" + id ;
                                        else{
                                            classroom_list = classroom_list + ',' + id;
                                        }
                                        document.getElementById('classroom_list').value = classroom_list;
                                    }
                                </script>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($classrooms as $key => $classroom)
                                        <button type="button" class="dropdown-item" onclick="add_classroom({{$key+1}},{{$classroom['classroom']}},'{{$classroom['letter']}}')">
                                            {{$classroom['classroom']}} {{$classroom['letter']}}
                                        </button>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row" style="height:30px;margin-left:0px;">
                            <p class="text-muted">Classrooms selected :&nbsp;</p>
                            <p id="classroom_selected" class="font-weight-bold"></p>
                            <input id="classroom_list" name="classroom_list" style="display:none;">
                        </div>
                        <div class="form-group row align-items-center" style="margin-left:0px;">
                            <label for="chapter" class="mb-0">Select chapter:</label>
                            <input type="text" id="chapter" style="display:none;">
                            <div class="dropdown ml-3">
                                <button id="chapter_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Chapters
                                </button>
                                <script>
                                    function add_chapter(id,name){
                                        document.getElementById('chapter_selected').innerHTML = 'Chapter selected : '+ name;
                                        document.getElementById('chapter_id_selected').value = id;
                                    }
                                </script>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach ($chapters as $key => $chapter)
                                        <button type="button" class="dropdown-item" onclick="add_chapter({{$chapter['id']}},'{{$chapter['name']}}')">
                                            {{$chapter['name']}}
                                        </button>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row" style="height:30px;margin-left:0px;">
                            <p class="text-muted">Chapter selected :&nbsp;</p>
                            <p id="chapter_selected" class="font-weight-bold"></p>
                            <input id="chapter_id_selected" name="chapter_id_selected" style="display:none;">
                        </div>
                        <div class="form-group">
                            <label for="last_day">Select Last Day</label>
                            <input type="date" class="form-control" id="last_day" name="last_day">
                        </div>
                        <div class="form-group">
                            <label for="project_title">Project Title</label>
                            <input type="text" class="form-control" id="project_title" name="project_title" placeholder="Title">
                        </div>
                        <div class="form-group">
                            <label for="project_description">Project Description</label>
                            <textarea rows="3" cols="100" class="form-control" id="project_description" name="project_description" placeholder="Description"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" id="send_project">Assign Project</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
