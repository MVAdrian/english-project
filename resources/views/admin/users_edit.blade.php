@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">User Edit: {{$user['name']??"-"}}</h5>
                    <div class="card-body">
                            <a class="btn btn-primary mb-2" href="/users">Back to users</a>
                        <form method="POST" action="/users/{{$user['id']}}">
                            @csrf
                            <div>
                                <label class="font-weight-bold" for="name">Name</label>
                                <input class="form-control" name="name" value="{{$user['name']}}">
                            </div>
                            <div>
                                <label class="font-weight-bold" for="email">Email</label>
                                <input class="form-control" name="email" value="{{$user['email']}}">
                            </div>
                            <button class="btn btn-primary mt-4" type="submit">Update</button>
                        </form>
                        <h5 class="my-2">!! User default password : "<strong>kimobe_user</strong>"</h5>
                        <a class="btn btn-danger" target="_blank" href="/users/reset/{{$user['id']}}">Reset user password</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
