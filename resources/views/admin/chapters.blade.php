@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="error-holder">
                @foreach ($errors->all() as $error)
                    <div class="error-handler" id="error-handler">{{ $error }}</div>
                @endforeach
            </div>
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">Admin Panel - Chapters</h5>
                    <div class="card-body">
                        <div class="row ml-0 align-items-center">
                            <a class="btn btn-primary ml-auto mr-2 mb-2" href="/create_chapter">Add Chapter</a>
                        </div>
                        <script>

                            function ConfirmDelete()
                            {
                                var x = confirm("Are you sure you want to delete?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }

                        </script>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Chapter</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($chapters as $chapter)
                                <tr>
                                    <td class="w-25">{{$chapter['name']}}</td>
                                    <td class="ellipsis w-50">{{$chapter['description']}}</td>
                                    <td class="row mx-0">
                                        <a class="btn btn-primary mr-2 mb-2" href="/chapters_admin/{{$chapter['id']}}">Edit</a>
                                        <form method="POST" action="/chapters_admin/delete/{{$chapter['id']}}" onsubmit="return ConfirmDelete()">
                                            @csrf
                                            <button class="btn btn-danger mb-2" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
