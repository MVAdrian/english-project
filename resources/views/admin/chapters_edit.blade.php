@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="card-header bg-primary text-white">Update Chapter</h4>
                    <div class="card-body">
                        <form method="POST" action="/chapters_admin/{{$chapter['id']}}">
                            @csrf
                            <div>
                                <label for="chapter_name">Insert chapter's name</label>
                                <input class="form-control" id="chapter_name" name="chapter_name" placeholder="Name" value="{{$chapter['name']}}" required>
                            </div>
                            <div class="mt-3">
                                <label for="chapter_name">Insert chapter's description</label>
                                <textarea class="form-control" rows="4"  id="chapter_description" name="chapter_description" placeholder="Description" required>{{$chapter['description']}}</textarea>
                            </div>
                            <button class="form-control btn btn-primary mt-3" type="submit">Update Chapter</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
