@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">New User</h5>
                    <div class="card-body">
                            <a class="btn btn-primary mb-2" href="/users">Back to users</a>
                        <form method="POST" action="/users/add">
                            @csrf
                            <div>
                                <label class="font-weight-bold" for="name">Name</label>
                                <input class="form-control" name="name" required>
                            </div>
                            <div>
                                <label class="font-weight-bold" for="email">Email</label>
                                <input class="form-control" type="email" name="email" required>
                            </div>
                            <h5 class="my-2">!! User default password : "<strong>kimobe_user</strong>"</h5>
                            <button class="btn btn-primary" type="submit">Create User</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
