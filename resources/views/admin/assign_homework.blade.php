@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header bg-primary font-weight-bold text-white">Assign Homework</h5>
                    <div class="card-body">
                        <form id="homework" method="POST" action="{{route('set_homework')}}">
                            @csrf
                                <div class="dropdown">
                                    <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Chapters
                                    </button>
                                    <h5 class="mt-2" id="chapter_text">Chapter selected: None</h5>
                                    <script>
                                        function select_homework(e){
                                            document.getElementById('homework_text').innerHTML = "Homework selected: " + e.innerHTML;
                                            console.log(e.id);
                                            document.getElementById('homework_id').value = e.id;
                                        }
                                        function select_chapter_list(id,name){
                                            document.getElementById('chapter_text').innerHTML = "Chapter selected: "+name;
                                            csrf_token = document.getElementsByName('csrf-token')[0].content;
                                            $.ajaxSetup({
                                                headers: {
                                                    'X-CSRF-TOKEN': csrf_token
                                                }
                                            });
                                            jQuery.ajax({
                                                    url: "{{ url('/return_homeworks') }}",
                                                method: 'post',
                                                data: {
                                                    chapter_id: id,
                                                },
                                                success: function(result){
                                                        var btn = document.getElementById('homework_example');
                                                        for(var i = 0; i < result.length ; i++){
                                                            var clone = btn.cloneNode(true);
                                                            clone.id = result[i]['id'];
                                                            clone.innerHTML = result[i]['title'];
                                                            document.getElementById('homeworks_holder').appendChild(clone);
                                                        }
                                                    console.log(result);
                                                }});

                                        }
                                    </script>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        @foreach ($chapters as $key => $chapter)
                                            <button type="button" class="dropdown-item" onclick="select_chapter_list({{$chapter['id']}},'{{$chapter['name']}}')">
                                                {{$chapter['name']}}
                                            </button>
                                        @endforeach
                                    </div>
                                </div>
                            <div class="dropdown mt-4">
                                <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Homeworks
                                </button>
                                <h5 class="mt-2" id="homework_text">Homework selected: None</h5>
                                <div class="dropdown-menu" id="homeworks_holder" aria-labelledby="dropdownMenuButton2">
                                        <button type="button" id="homework_example" class="dropdown-item" onclick="select_homework(this)">
                                            Select a homework
                                        </button>
                                </div>
                            </div>
                            <input id="homework_id" name="homework_id" class="hide">
                            <div class="form-group row align-items-center" style="margin-left:0px;">
                                <input type="text" id="classroom" style="display:none;">
                                <div class="dropdown">
                                    <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Classrooms
                                    </button>
                                    <script>
                                        function add_classroom(id,classroom,letter){
                                            var text = document.getElementById('classroom_selected').innerHTML;
                                            text = text + classroom +'-'+ letter + " ; ";
                                            document.getElementById('classroom_selected').innerHTML = text;

                                            var classroom_list = document.getElementById('classroom_list').value;
                                            if(classroom_list == "")
                                                classroom_list = "[" + id ;
                                            else{
                                                classroom_list = classroom_list + ',' + id;
                                            }
                                            document.getElementById('classroom_list').value = classroom_list;
                                        }
                                    </script>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($classrooms as $key => $classroom)
                                            <button type="button" class="dropdown-item" onclick="add_classroom({{$classroom['id']}},{{$classroom['classroom']}},'{{$classroom['letter']}}')">
                                                {{$classroom['classroom']}} {{$classroom['letter']}}
                                            </button>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="height:30px;margin-left:0px;">
                                <h5 class="text-muted">Classrooms selected :&nbsp;</h5>
                                <p id="classroom_selected" class="font-weight-bold"></p>
                                <input id="classroom_list" name="classroom_list" style="display:none;">
                            </div>
                            <div class="form-group">
                                <label for="last_day">Select Last Day</label>
                                <input type="date" class="form-control" id="last_day" name="last_day">
                            </div>
                            <button type="submit" class="btn btn-primary form-control">Send Homework</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

