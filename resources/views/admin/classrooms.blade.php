@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="error-holder">
                @foreach ($errors->all() as $error)
                    <div class="error-handler" id="error-handler">{{ $error }}</div>
                @endforeach
            </div>
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">Admin Panel - Classrooms</h5>
                    <div class="card-body">
                        <div class="row ml-0 align-items-center">
                            <a class="btn btn-primary ml-auto mr-2 mb-2" href="/classrooms_admin/add">Add Classroom</a>
                        </div>
                        <script>

                            function ConfirmDelete()
                            {
                                var x = confirm("Are you sure you want to delete?");
                                if (x)
                                    return true;
                                else
                                    return false;
                            }

                        </script>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Number</th>
                                    <th>Letter</th>
                                    <th class="text-right pr-4">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($classrooms as $classroom)
                                <tr>
                                    <td class="w-25">{{$classroom['classroom']}}</td>
                                    <td class="w-25">{{$classroom['letter']}}</td>
                                    <td class="row mx-0 justify-content-end">
                                        <a class="btn btn-primary mr-2 mb-2" href="/classrooms_admin/{{$classroom['id']}}">Edit</a>
                                        <form method="POST" action="/classrooms_admin/delete/{{$classroom['id']}}" onsubmit="return ConfirmDelete()">
                                            @csrf
                                            <button class="btn btn-danger mb-2" type="submit">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
