@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">Edit Secret Key</h5>
                    <div class="card-body">
                            <a class="btn btn-primary mb-2" href="/admin">Back to Admin Page</a>
                        <form method="POST" action="/secret_key/edit">
                            <h5 class="font-weight-bold my-3">Current key : "{{ $secret_key }}"</h5>
                            @csrf
                            <div>
                                <label class="font-weight-bold" for="secret_key">New Secret Key</label>
                                <input class="form-control" id="secret_key" name="secret_key" required>
                            </div>
                            <button class="btn btn-outline-danger mt-4" type="submit">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
