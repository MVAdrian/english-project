@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <h4 class="card-header bg-primary text-white">Correct Assignments</h4>
                        <div class="card-body">
                            <div class="row ml-0 w-100">
                                <div class="dropdown mr-2">
                                    <script>
                                        function selectClassroom(id) {
                                            console.log(id);
                                            $.ajax({
                                                url: '/getAssignmentsByClassroomId',
                                                type: 'post',
                                                data: {id: id},
                                                headers: { "Access-Control-Allow-Methods": "POST",
                                                    "Allow": "POST",
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                                },
                                                success: function(response){
                                                    $('#assignmentsDropdown').html(response);
                                                }
                                            });
                                        }

                                        function selectHomework(classroom_id,homework_id){
                                            console.log(classroom_id,homework_id);
                                            $.ajax({
                                                url: '/getAssignmentsList',
                                                type: 'post',
                                                data: {classroom_id: classroom_id,homework_id:homework_id},
                                                headers: { "Access-Control-Allow-Methods": "POST",
                                                    "Allow": "POST",
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                                },
                                                success: function(response){
                                                    $('#assignmentsResults').html(response);
                                                }
                                            });
                                        }
                                    </script>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Classroom
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach($classrooms as $classroom)
                                            <button class="dropdown-item" onclick="selectClassroom({{$classroom['id']}})">{{$classroom['classroom'].' '.$classroom['letter']}}</button>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="dropdown mr-2">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Select Homework
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" id="assignmentsDropdown">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
{{--                                <button class="btn btn-primary ml-auto">Show results</button>--}}
                            </div>
                            <div class="col-md-12 mt-4" id="assignmentsResults">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
