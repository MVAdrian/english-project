@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card w-100">
                    <h5 class="card-header text-white bg-primary">Admin Panel</h5>
                    <div class="card-body text-center">
                        <h4>Welcome! What are we doing today ?</h4>
                        <a class="btn btn-primary w-50 mt-4" href="{{route('user_listing')}}">Users</a>
                        <a class="btn btn-primary w-50 mt-4" href="{{route('classrooms_admin')}}">Classrooms</a>
                        <a class="btn btn-primary w-50 mt-4" href="{{route('chapters_admin')}}">Chapters</a>
                        <a class="btn btn-primary w-50 mt-4" href="{{route('lessons_admin')}}">Lessons</a>
                        <a class="btn btn-primary w-50 mt-4" href="{{route('homework_admin')}}">Homeworks</a>
                        <a class="btn btn-outline-danger w-50 mt-4" href="{{route('secret_key')}}">Edit Registration Secret Key</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
