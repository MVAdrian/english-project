@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h4 class="card-header bg-primary text-white">Create Lesson</h4>
                    <div class="card-body">
                        <form method="POST" action="{{route('assign_lesson')}}">
                            @csrf
                            <div class="form-row align-items-center ml-0 mb-1">
                                <label for="lesson_chapter_selected" class="mb-0">Select lesson's chapter</label>
                                <div class="dropdown ml-3">
                                    <button id="classroom_button" class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Chapters
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        @foreach ($chapters as $key => $chapter)
                                            <button type="button" class="dropdown-item" onclick="select_chapter({{$chapter['id']}},'{{$chapter['name']}}')">
                                                {{$chapter['name']}}
                                            </button>
                                        @endforeach
                                    </div>
                                    <script>
                                        function select_chapter(id,name){
                                            document.getElementById('lesson_chapter_selected').placeholder = name;
                                            document.getElementById('lesson_chapter_selected').value = name;
                                            document.getElementById('lesson_chapter').value = id;
                                        }

                                        function insertBoldText(){
                                            let old_text = document.getElementById('lesson_info').value;
                                            let bold_text = " <strong>" + document.getElementById('bold_text').value + "</strong> ";
                                            let new_text = old_text + bold_text;
                                            document.getElementById('lesson_info').value = new_text;
                                            document.getElementById('bold_text').value = "";
                                        }
                                        function insertLinkText(){
                                            let old_text = document.getElementById('lesson_info').value;
                                            let link_text = "<br/><a href="+'"'+document.getElementById('link_text').value+'"'+" target='_blank'>" + document.getElementById('link_description').value + "</a><br/>";
                                            let new_text = old_text + link_text;
                                            document.getElementById('lesson_info').value = new_text;
                                            document.getElementById('link_text').value = "";
                                            document.getElementById('link_description').value = "";
                                        }
                                    </script>
                                </div>
                            </div>
                            <input class="form-control mb-3 hide" id="lesson_chapter" name="lesson_chapter" required>
                            <input class="form-control mb-3" id="lesson_chapter_selected" placeholder="Chapter selected" required disabled>
                            <div>
                                <label for="lesson_name">Insert lesson's name</label>
                                <input class="form-control" id="lesson_name" name="lesson_name" placeholder="Name" required>
                            </div>
                            <div class="mt-3">
                                <label for="lesson_description">Insert lesson's description</label>
                                <textarea class="form-control" rows="4"  id="lesson_description" name="lesson_description" placeholder="Description" required></textarea>
                            </div>
                            <div class="mt-3">
                                <label for="lesson_description">Insert <strong> BOLD </strong>text into lesson</label>
                                <div class="row ml-0 align-items-center">
                                    <input id="bold_text" class="form-control w-50 mr-2">
                                    <button class="btn btn-primary" onclick="insertBoldText()" type="button">Insert</button>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="lesson_description">Insert a <a href="#"> LINK </a> into lesson</label>
                                <div class="row ml-0 align-items-center">
                                    <input id="link_text" class="form-control w-50 mr-2" placeholder="Insert Link Here">
                                    <input id="link_description" class="form-control w-50 mr-2" placeholder="Insert a description for link">
                                    <button class="btn btn-primary" onclick="insertLinkText()" type="button">Insert</button>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="lesson_info">Insert lesson's info</label>
                                <textarea class="form-control" rows="10"  id="lesson_info" name="lesson_info" placeholder="Info" required></textarea>
                            </div>
                            <button class="form-control btn btn-primary mt-3" type="submit">Assign Lesson</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
