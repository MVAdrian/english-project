@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="error-holder">
                @foreach ($errors->all() as $error)
                    <div class="error-handler" id="error-handler">{{ $error }}</div>
                @endforeach
            </div>
            <div class="col-md-12">
                <div class="card">
                    <h4 class="card-header bg-primary text-white">Update Classroom</h4>
                    <div class="card-body">
                        <form method="POST" action="{{route('classrooms_admin.update',$classroom['id'])}}">
                            @csrf
                            <div>
                                <label for="classroom_number">Insert classroom's number</label>
                                <input class="form-control" id="classroom_number" name="classroom_number" placeholder="1, 2, 3 ..." type="number" value="{{$classroom['classroom']}}" required>
                            </div>
                            <div>
                                <label for="classroom_letter">Insert classroom's letter</label>
                                <input class="form-control" id="classroom_letter" name="classroom_letter" placeholder="A, B, C ..." type="text" value="{{$classroom['letter']}}" required>
                            </div>
                            <button class="form-control btn btn-primary mt-3" type="submit">Update Classroom</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
