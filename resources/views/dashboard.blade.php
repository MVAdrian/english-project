@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
        <div class="error-holder">
            @foreach ($errors->all() as $error)
                <div class="error-handler" id="error-handler">{{ $error }}</div>
            @endforeach
        </div>
		<div class="col-md-3 my-4">
			<div class="card">
				<h5 class="card-header font-weight-bold bg-primary text-white">
					About Me
				</h5>
				<div class="card-body card-min-height">
					<p>
						Full Name : {{ Auth::user()->name}}
					</p>
					<p>
						@if(isset(Auth::user()->classroom_id))
							Classroom: {{$class['classroom']}} {{$class['letter']}}
						@else
							Please update your classroom.
						@endif
					</p>
					<button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal3">Update my info</button>
					<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModal3Label" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModal3Label">Update info</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="POST" action="{{ route('updateUserData') }}">
										@csrf

										<div class="form-group row">
											<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

											<div class="col-md-6">
												<input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name}}" required autocomplete="name" autofocus>

												@error('name')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
												@enderror
											</div>
										</div>

										<div class="form-group row">
											<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

											<div class="col-md-6">
												<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email}}" required autocomplete="email">

												@error('email')
													<span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
												@enderror
											</div>
										</div>
                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                            <div class="col-md-6">
                                                <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">

                                                @error('password_confirmation')
                                                <span class="invalid-feedback" role="alert">
														<strong>{{ $message }}</strong>
													</span>
                                                @enderror
                                            </div>
                                        </div>
										<div class="form-group row d-flex justify-content-center">
											<input id="user" name="user" style="display:none;" value="{{json_encode(Auth::user())}}">
											<input id="classroom_id" name="classroom_id" style="display:none;">
											<div class="dropdown">
												<button id="classroom_button" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													Select your classroom
												</button>
												<script>
													function change_text(id,classroom,letter){
														var title = classroom.toString();
														title += " ";
														title += letter;
														document.getElementById('classroom_button').innerHTML = title;
														document.getElementById('classroom_id').value = id;
													}
												</script>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													@foreach ($classrooms as $key => $classroom)
														<button type="button" class="dropdown-item" onclick="change_text({{$classroom['id']}},{{$classroom['classroom']}},'{{$classroom['letter']}}')">
															{{$classroom['classroom']}} {{$classroom['letter']}}
														</button>
													@endforeach
												</div>
											</div>
										</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button type="submit" class="btn btn-primary">Save changes</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9 my-4">
			<div class="card">
				<h5 class="card-header font-weight-bold bg-primary text-white">
					Achivements obtained
				</h5>
				<div class="card-body card-min-height">
					<img src="/assets/achivements-activated/001.png" alt="Achivement 1" class="achivement-dashboard">
					<img src="/assets/achivements-activated/002.png" alt="Achivement 2" class="achivement-dashboard">
					<img src="/assets/achivements/003.png" alt="Achivement 3" class="achivement-dashboard">
					<img src="/assets/achivements/004.png" alt="Achivement 4" class="achivement-dashboard">
					<img src="/assets/achivements/005.png" alt="Achivement 5" class="achivement-dashboard">
					<img src="/assets/achivements/006.png" alt="Achivement 6" class="achivement-dashboard">
					<img src="/assets/achivements/007.png" alt="Achivement 7" class="achivement-dashboard">
					<img src="/assets/achivements/008.png" alt="Achivement 8" class="achivement-dashboard">
					<img src="/assets/achivements/009.png" alt="Achivement 9" class="achivement-dashboard">
					<img src="/assets/achivements/010.png" alt="Achivement 10" class="achivement-dashboard">

					<div class="font-weight-bold my-2 ml-2">
						<a class="btn btn-primary mt-2" href="{{route('achivements')}}">
							Achivements Details
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 my-4">
			<div class="card">
				<h5 class="card-header font-weight-bold bg-primary text-white">
					Homework Assignments
				</h5>
				<div class="card-body">
                    @if(isset($homeworks[0]))
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Title</th>
                                    <th scope="col">Due Date</th>
                                    <th scope="col">Chapter</th>
                                    <th scope="col">Start Homework</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($homeworks as $key => $homework)
                                    <tr>
                                        <th scope="row">{{$homework['title']}}</th>
                                        <td>{{date('Y-m-d',strtotime($homework['end_date']))}}</td>
                                        <td class="font-weight-bold">{{$homework['name']}}</td>
                                        <td><a class="btn btn-dark" href="{{route('complete_homework',['homework_id' => $homework['homework_id']])}}">Begin Homework</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <h5>No assignments for now!</h5>
                    @endif
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 text-right">
		Achivements icons made by <a href="https://www.flaticon.com/authors/geotatah" title="geotatah">geotatah</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
	</div>
</div>
@endsection
