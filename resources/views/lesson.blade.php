@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header bg-primary text-white">{{$lesson['name']}}</h5>
                    <div class="card-body">
                        <div class="mb-3">
                            <h6 class="font-weight-bold">Description</h6>
                            <p>{{$lesson['description']}}</p>
                        </div>
                        <div>
                            <h6 class="font-weight-bold">Lesson</h6>
                            <?php
                            echo nl2br($lesson['info']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
