@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col">
            <div class="row">
                <div class="col-md-12 align-items-center justify-content-center">
                    <h2 class="text-center font-weight-bold">{{$project['project_title']}} : Completed</h2>
                </div>
                <div class="col-md-12">
                    <div class="w-100 d-flex flex-column justify-content-center align-items-center">
                        <div class="my-3 p-3" style="width:80%;background-color:white;border-radius: 10px">
                            <h4 class="mb-0">{{$project['project_description']}}</h4>
                            <form method="post" action="{{route('download_project')}}" class="mt-3">
                                @csrf
                                <div class="form-group">
                                    <input class="form-control hide" name="filename" id="filename" type="text" value="{{$upload['file_name']}}">
                                </div>
                                <button type="submit" class="btn btn-success">Download Project</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
