@extends('layouts.app')

@section('content')
    <div class="col-md-12 align-items-center justify-content-center">
        <h2 class="text-center font-weight-bold">{{$project['project_title']}}</h2>
    </div>
    <div>
        {{Form::open(array('url' => '/upload_project','files' => true,'method'=>'post'))}}

        @csrf
            <div class="w-100 d-flex flex-column justify-content-center align-items-center">
                <div class="my-3 p-3" style="width:80%;background-color:white;border-radius: 10px">
                    <h4 class="mb-0">{{$project['project_description']}}</h4>
                    <div class="form-group align-items-center justify-content-center mt-2" style="height:40px;">
                        <label for="project_file" class="mb-0 form-control">Upload your project here:</label>
                        <input type="file" id="project_file" class="form-control" name="project_file">
                    </div>
                    <input class="hide" value="{{$_GET['project_id']}}" name="project_id" id="project_id">
                    <button type="submit" class="btn btn-primary mt-5">Send project</button>
                </div>
            </div>
        {{Form::close()}}
    </div>
@endsection
