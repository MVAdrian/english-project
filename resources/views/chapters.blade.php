@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header bg-primary text-white">Chapters</h5>
                <div class="card-body">
                    @if(isset($chapters))
                        @foreach($chapters as $key => $chapter)
                            <div class="mb-5">
                                <h6 class="font-weight-bold mb-2">{{$chapter['name']}}</h6>
                                <p>{{$chapter['description']}}</p>
                                <a class="btn btn-primary" href="/lessons/{{$chapter['id']}}">Click to learn more about this chapter</a>
                            </div>
                        @endforeach
                        @else
                        <h5>No chapters uploaded.</h5>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
