<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Chapter;
use App\Completed;
use App\Homework;
use Illuminate\Http\Request;

class HomeworkCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $homeworks = Homework::select('homeworks.id','homeworks.title','chapter_id','chapters.name as chapter_name');
        $homeworks = $homeworks->leftJoin('chapters','chapters.id','homeworks.chapter_id');

        if(isset($_GET['chapter_id'])){
            $homeworks = $homeworks->where('homeworks.chapter_id',$_GET['chapter_id']);
        }
        $homeworks = $homeworks->get()->toArray();
        $chapters = Chapter::get();

        return view('admin.homeworks',compact('homeworks','chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapters = Chapter::get();
        $homework = Homework::where('id',$id)->first();
        dd(json_decode($homework['json'],true));
        $homework_chapter = Chapter::where('id',$homework['chapter_id'])->first();
//        dd($lesson['description']);
        return view('admin.update_homework',compact('chapters','homework','homework_chapter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Homework::where('id',$id)->delete();
        Assignment::where('homework_id',$id)->delete();
        Completed::where('homework_id',$id)->delete();
    }
}
