<?php

namespace App\Http\Controllers;

use App\AppSettings;
use App\Assignment;
use App\Chapter;
use App\Classroom;
use App\Completed;
use App\Homework;
use App\Lesson;
use App\User;
use Illuminate\Http\Request;

class AdminCrudController extends Controller
{
    public function secret()
    {
        $secret_key = AppSettings::where('slug','secret_key')->first();
        return view('admin.secret')->with('secret_key',$secret_key['value']);
    }

    public function edit_secret()
    {
        $new_key = $_POST['secret_key'];
        AppSettings::where('slug','secret_key')->update([
            'value' => $new_key
        ]);
        $secret_key = AppSettings::where('slug','secret_key')->first();
        return view('admin.secret')->with('secret_key',$secret_key['value']);
    }
    public function correct_assignments()
    {
        $classrooms = Classroom::get();
        return view('admin.visual.assignments')->with('classrooms',$classrooms);
    }

   public function create_chapter()
   {
        return view('admin.create_chapter');
   }

   public function assign_chapter()
   {
        $name = $_POST['chapter_name'];
        $description = $_POST['chapter_description'];
        Chapter::create([
            'name' => $name,
            'description' => $description
            ]
        );
        return redirect('/chapters_admin');
   }

   public function create_lesson()
   {
       $chapters = Chapter::get();
       return view('admin.create_lesson',compact('chapters'));
   }

   public function assign_lesson()
   {
       $chapter_id = $_POST['lesson_chapter'];
       $name = $_POST['lesson_name'];
       $description = $_POST['lesson_description'];
       Lesson::create([
            'chapter_id' => $chapter_id,
           'name' => $name,
           'description' => $description,
           'info' => $_POST['lesson_info']
       ]);
       return redirect('/lessons_admin');
   }

   public function show_admin()
   {
       return view('admin.admin');
   }

   public function view_homework($user_id,$homework_id)
   {
//       $id = $_GET['homework_id'];
       $user = User::where('id',$user_id)->first();
       $answer = Completed::where('user_id',$user_id)->where('homework_id',$homework_id)->get();
//        dd($answer);
       $info = Homework::where('id',$homework_id)->get()[0];
       $homework = json_decode($info['json'],true);
       if(isset($answer[0])){
           $assign = Assignment::where('homework_id',$homework_id)->where('classroom_id',$user->classroom_id)->get()[0];
           $final = json_decode($answer[0]['final'],true);
           if(!isset($final['essay'])){
               $final['essay'] = 0;
           }
           $json = json_decode($answer[0]['json'],true);

           $grid_answers = [];
           $text_answers = [];
           $variants_answers = [];
           $answers = [];
           if(isset($json['answer_grid'])) {
               $answers['grid_answers'] = $json['answer_grid'];
           }
           if(isset($json['answer_text'])) {
               $answers['text_answers'] = $json['answer_text'];
           }
           if(isset($json['answer_variants'])) {
               $answers['variants_answers'] = $json['answer_variants'];
           }
           if(isset($json['answer_essay'])) {
               $answers['essay_answers'] = $json['answer_essay'];
           }
           $answer_id = $answer[0]['id'];
           return view('completed_homework',compact('homework','final','answers','answer_id'));
       }
       else
           return back();
   }
}
