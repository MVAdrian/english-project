<?php

namespace App\Http\Controllers;

use App\AppSettings;
use App\Assignment;
use App\Chapter;
use App\Completed;
use App\Homework;
use App\Project;
use App\Uploaded;
use Cassandra\Time;
use Illuminate\Http\Request;
use App\Classroom;
use Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
        $this->middleware('auth');
     }

     /**
      * Show the application dashboard.
      *
      * @return \Illuminate\Contracts\Support\Renderable
      */
     public function index()
     {
         if(Auth::user()->is_admin == 1){
             return redirect('/admin');
         }
//         dd($_REQUEST);
        $classrooms = Classroom::get();
        if(isset(Auth::user()->classroom_id)){
            $class = Classroom::where('id',Auth::user()->classroom_id)->get()[0];
            $homeworks = Assignment::select(
                'assignments.id','homework_id','assignments.classroom_id','end_date','homeworks.json','chapters.name','homeworks.title'
            )
            ->join('homeworks','homeworks.id','assignments.homework_id')
            ->join('chapters','chapters.id','homeworks.chapter_id')
            ->where('end_date','>=',date('Y-m-d h:s:m'))
            ->where('assignments.classroom_id',Auth::user()->classroom_id)->get();
//            dd($homeworks);
             $projects = Project::select(
                 'projects.id',
                 'projects.end_date',
                 'chapters.name'
             )->join('chapters','chapters.id','projects.chapter_id')->where('classroom_id',Auth::user()->classroom_id)->get();
//            dd($projects,Auth::user()->classroom_id);
//            $projects = Project::where('id',11)->get();
            return view('dashboard',compact('classrooms','class','homeworks','projects'));
        }
        else{
            return view('dashboard',compact('classrooms'));
        }
     }

     public function correct_homework()
     {
         $homework_id = $_POST['homework_id'];
         $homework = json_decode(Homework::where('id',$homework_id)->get()[0]['json'],true);
         $grid_correction = 0;
         $text_correction = 0;
         $variants_correction = 0;
         $answers = [];

         if(isset($_POST['answer_grid'])){
             $grid_questions_number = count($homework['question']['grid']);
             $grid_answers = $_POST['answer_grid'];
             $answers['grid_answers'] = $grid_answers;
             foreach($_POST['answer_grid'] as $grid_question_value => $answer){
                 if($answer == $homework['answer']['grid'][$grid_question_value][$homework['correct_answer']['grid'][$grid_question_value]]){
                     $grid_correction += 100;
                 }
                 else{
                     $grid_correction += 0;
                 }
             }
             $final_grid = round(($grid_correction/$grid_questions_number),2);
         }
         if(isset($_POST['answer_text'])){
             $text_questions_number = count($homework['question']['text']);
             $text_answers = $_POST['answer_text'];
             $answers['text_answers'] = $text_answers;
             foreach($_POST['answer_text'] as $text_question_value => $answer){
                 $percent = 0;
                 $similarity = similar_text($answer,$homework['answer']['text'][$text_question_value][1],$percent);
                 $text_correction += $percent;
             }
             $final_text = round(($text_correction/$text_questions_number),2);
         }

         if(isset($_POST['answer_variants'])){
             $variants_questions_number = 0;
             $variants_answers = $_POST['answer_variants'];
             $answers['variants_answers'] = $variants_answers;
             foreach ($_POST['answer_variants'] as $variants_question_value => $answers_array){
                 foreach ($answers_array as $answer_key => $answer){
                     $variants_questions_number++;
                     if($answer == $homework['variants'][$variants_question_value][$answer_key][$homework['variants'][$variants_question_value][$answer_key][2] - 1]){
                         $variants_correction += 100;
                     }
                 }
             }
             $final_variants = round(($variants_correction/$variants_questions_number),2);
         }

         $final_sum = 0;
         $cnt = 0;
         if(isset($_POST['answer_grid'])) {
             $final_sum += $final_grid;
             $cnt++;
         }
         if(isset($_POST['answer_text'])) {
             $final_sum += $final_text;
             $cnt++;
         }
         if(isset($_POST['answer_variants'])) {
             $final_sum += $final_variants;
             $cnt++;
         }
         if($cnt)
            $final_sum = round($final_sum/$cnt,2);
         else
             $final_sum = 0;

         $final = [
            'grid' => isset($_POST['answer_grid'])?$final_grid:0,
            'text' => isset($_POST['answer_text'])?$final_text:0,
            'variants' => isset($_POST['answer_variants'])?$final_variants:0,
            'essay' => 0,
            'total'=> $final_sum
         ];

         Completed::create([
             'user_id' => Auth::user()->id,
             'homework_id' => $homework_id,
             'final' => json_encode($final),
             'json' => json_encode($_POST),
             'essay_corrected' => isset($homework['essay'])?0:1,
         ]);

         return redirect('/complete_homework?homework_id='.$homework_id);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create_homework()
    {
        $classrooms = Classroom::get();
        $chapters = Chapter::get();
        return view('admin.create_homework',compact('classrooms','chapters'));
    }

    public function assign_homework()
    {
//        $classroom_list = json_decode($_POST['classroom_list']."]");
        $homework_title = $_POST['homework_title'];
//        $last_day = $_POST['last_day'];

//        foreach ($classroom_list as $key => $classroom_id){
            Homework::create([
                'title' => $homework_title,
                'chapter_id' => $_POST['chapter_id_selected'],
                'json' => json_encode($_POST),
            ]);
//        }

        return redirect('/homework_admin');
    }

    public function preview_homework()
    {
        $chapters = Chapter::get();
        $classrooms = Classroom::get();
        return view('admin.assign_homework',compact('chapters','classrooms'));
    }

    public function set_homework()
    {
        $classrooms_list = json_decode($_POST['classroom_list'].']',true);

        foreach($classrooms_list as $classroom_id) {
            Assignment::create([
                'classroom_id' => $classroom_id,
                'homework_id' => $_POST['homework_id'],
                'end_date' => $_POST['last_day']
            ]);
        }

        return redirect('/homework_admin');
    }

    public function return_homeworks()
    {
        $homeworks = Homework::select('id','title')->where('chapter_id',$_POST['chapter_id'])->get();
        return $homeworks;
    }

    public function complete_homework()
    {
        $id = $_GET['homework_id'];
        $answer = Completed::where('user_id',Auth::user()->id)->where('homework_id',$id)->get();
//        dd($answer);
        $info = Homework::where('id',$id)->get()[0];
        $homework = json_decode($info['json'],true);
        if(isset($answer[0])){
            $assign = Assignment::where('homework_id',$id)->where('classroom_id',Auth::user()->classroom_id)->get()[0];
            if($assign['end_date'] >= date('Y-m-d h:s:m')){
                return redirect(route('home'))->withErrors(['not_yet' => 'Results available after due date ends.']);
            }
            $final = json_decode($answer[0]['final'],true);
            if(!isset($final['essay'])){
                $final['essay'] = 0;
            }
            $json = json_decode($answer[0]['json'],true);

            $grid_answers = [];
            $text_answers = [];
            $variants_answers = [];
            $answers = [];
            if(isset($json['answer_grid'])) {
                $answers['grid_answers'] = $json['answer_grid'];
            }
            if(isset($json['answer_text'])) {
                $answers['text_answers'] = $json['answer_text'];
            }
            if(isset($json['answer_variants'])) {
                $answers['variants_answers'] = $json['answer_variants'];
            }
            if(isset($json['answer_essay'])) {
                $answers['essay_answers'] = $json['answer_essay'];
            }
            $answer_id = $answer[0]['id'];
            return view('completed_homework',compact('homework','final','answers','answer_id'));
        }
        else
            return view('complete_homework',compact('homework','info','id'));
    }

    public function update_grades($ca_id)
    {
//        dd($_POST,$ca_id);
        $assignment = Completed::where('id',$ca_id)->first();
        $grades = json_decode($assignment['final'],true);
        $cnt = 0;
        $sum = 0;
        if(isset($_POST['grid'])) {
            $grades['grid'] = (int)$_POST['grid'];
            $cnt += 1;
            $sum += (int)$_POST['grid'];
        }
        if(isset($_POST['text'])) {
            $grades['text'] = (int)$_POST['text'];
            $cnt += 1;
            $sum += (int)$_POST['text'];
        }
        if(isset($_POST['variants'])) {
            $grades['variants'] = (int)$_POST['variants'];
            $cnt += 1;
            $sum += (int)$_POST['variants'];
        }
        if(isset($_POST['essay'])) {
            $grades['essay'] = (int)$_POST['essay'];
            $cnt += 1;
            $sum += (int)$_POST['essay'];
        }
//        dd($grades,$cnt,$sum,$sum/$cnt);
        if($cnt) {
            $grades['total'] = $sum/$cnt;
        }
        Completed::where('id',$ca_id)->update([
            'final' => json_encode($grades)
        ]);
        return back()->withInput();
    }

    public function create_project()
    {
        $classrooms = Classroom::get();
        $chapters = Chapter::get();
        return view('admin.create_project',compact('classrooms','chapters'));
    }

    public function assign_project()
    {
        $classroom_list = json_decode($_POST['classroom_list']."]");
        $project_title = $_POST['project_title'];
        $last_day = $_POST['last_day'];
        $chapter_id = $_POST['chapter_id_selected'];

        foreach ($classroom_list as $key => $classroom_id){
            Project::create([
                'end_date' => $last_day,
                'project_title' => $project_title,
                'json' => json_encode($_POST),
                'classroom_id' => $classroom_id,
                'chapter_id' => $chapter_id
            ]);
        }

        return back()->withInput();
    }

    public function complete_project()
    {
        $info = Project::where('id',$_GET['project_id'])->get()[0];
        $project = json_decode($info['json'],true);
        $isset = Uploaded::where('project_id',$info['id'])->where('user_id',Auth::user()->id)->get();
        if(isset($isset[0])){
            return view('uploaded_project')->with('project',$project)->with('upload',$isset[0]);
        }
        return view('complete_project',compact('project','info'));
    }

    public function upload_project(Request $request)
    {
        if(isset(Uploaded::where('project_id',$_POST['project_id'])->where('user_id',Auth::user()->id)->get()[0]))
            return back()->withInput();
        $file_name = $_FILES['project_file']['name'];
        $destinationPath = storage_path().'/uploads/';
        $path = $destinationPath.$file_name;

        Uploaded::create([
            'project_id' => $_POST['project_id'],
            'user_id' => Auth::user()->id,
            'file_name' => $file_name,
            'path' => $path
        ]);

        foreach($request->files as $formName => $csv){
            $file = $request->file($formName);
            $file->move($destinationPath,$file->getClientOriginalName());
        }
        return redirect('/complete_project?project_id='.$_POST['project_id']);
    }

    public function download_project()
    {
        $filename = $_POST['filename'];
        return response()->download(storage_path('uploads/' . $filename));
    }

    public function contact()
    {
        return view('contact');
    }

    public function contact_send()
    {
        $mail_to = AppSettings::where('slug','email')->first();
        $message = $_POST['first_name']."-".$_POST['last_name']." Phone number : ".$_POST['phone_number'].". Email : ".$_POST['email']." . Observation : ".$_POST['observations'];
        mail(
          $mail_to['value'],
          'Contact Form',
            $message
        );
        return redirect('/home');
    }



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
