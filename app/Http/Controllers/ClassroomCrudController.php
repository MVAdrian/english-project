<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Classroom;
use App\User;
use Illuminate\Http\Request;

class ClassroomCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $classrooms = Classroom::orderBy('classroom')->orderBy('letter')->get();
        return view('admin.classrooms',compact('classrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.create_classroom');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $classroom = $_POST['classroom_number'];
        $letter = $_POST['classroom_letter'];

        $check = Classroom::where('classroom',$classroom)->where('letter',$letter)->get()->toArray();

        if(empty($check)) {
            // create
            Classroom::create([
               'classroom' => $classroom,
               'letter' => $letter
            ]);
            return redirect('/classrooms_admin');
        } else {
            return back()->withErrors(['exists' => 'Classroom already exists.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $classroom = Classroom::where('id',$id)->first();
        return view('admin.update_classroom',compact('classroom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $classroom = $_POST['classroom_number'];
        $letter = $_POST['classroom_letter'];

        $check = Classroom::where('classroom',$classroom)->where('letter',$letter)->get()->toArray();

        if(empty($check)) {
            // create
            Classroom::where('id',$id)->update([
                'classroom' => $classroom,
                'letter' => $letter
            ]);
            return redirect('/classrooms_admin');
        } else {
            return back()->withErrors(['exists' => 'Classroom already exists.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::where('classroom_id',$id)->get()->toArray();
        $assignments = Assignment::where('classroom_id',$id)->get()->toArray();
        if(empty($assignments) && empty($users)){
            Classroom::where('id',$id)->delete();
            return redirect('/classrooms_admin');
        } else {
            return redirect('/classrooms_admin')->withErrors(['not_yet' => 'Classroom in use. Please check all the users and assignments sent.']);
        }
    }
}
