<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Lesson;
use Illuminate\Http\Request;

class LessonsCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lessons = Lesson::select('lessons.id','lessons.name','chapter_id','chapters.name as chapter_name');
        $lessons = $lessons->leftJoin('chapters','chapters.id','lessons.chapter_id');

        if(isset($_GET['chapter_id'])){
            $lessons = $lessons->where('lessons.chapter_id',$_GET['chapter_id']);
        }
        $lessons = $lessons->get()->toArray();
        $chapters = Chapter::get();

        return view('admin.lessons',compact('lessons','chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $chapters = Chapter::get();
        $lesson = Lesson::where('id',$id)->first();
        $lesson_chapter = Chapter::where('id',$lesson['chapter_id'])->first();
//        dd($lesson['description']);
        return view('admin.update_lesson',compact('chapters','lesson','lesson_chapter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chapter_id = $_POST['lesson_chapter'];
        $name = $_POST['lesson_name'];
        $description = $_POST['lesson_description'];
        Lesson::where('id',$id)->update([
            'chapter_id' => $chapter_id,
            'name' => $name,
            'description' => $description,
            'info' => $_POST['lesson_info']
        ]);
        return redirect('/lessons_admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lesson::where('id',$id)->delete();
        return redirect('/lessons_admin');
    }
}
