<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\Homework;
use App\Lesson;
use Illuminate\Http\Request;

class ChaptersCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $chapters = Chapter::get();
        return view('admin.chapters',compact('chapters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $chapter = Chapter::where('id',$id)->first();
        return view('admin.chapters_edit',compact('chapter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        Chapter::where('id',$id)->update([
           'name' => $_POST['chapter_name'],
           'description' => $_POST['chapter_description']
        ]);
        return redirect('/chapters_admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $homeworks = Homework::where('chapter_id',$id)->get()->toArray();
        $lessons = Lesson::where('chapter_id',$id)->get()->toArray();
        if(empty($lessons) && empty($homeworks)){
            Chapter::where('id',$id)->delete();
            return redirect('/chapters_admin');
        } else {
            return redirect('/chapters_admin')->withErrors(['not_yet' => 'Chapter in use. Please check all the lessons and assignments created.']);
        }
    }
}
