<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\Chapter;
use App\Classroom;
use App\Homework;
use App\Lesson;
use Illuminate\Support\Facades\Hash;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\ProjectController;
use App\Achivement;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserCrudController extends Controller
{

    public function assignments_history()
    {
        if(isset(Auth::user()->classroom_id)){
//        $all_assignments = Assignment::select(
//            [
//                'homeworks.title',
//                'chapters.name',
//                'end_date',
//                'homeworks.id',
//                'completed_assignments.user_id'
//            ]
//        )
//            ->join('homeworks','assignments.homework_id','homeworks.id')
//            ->join('chapters','homeworks.chapter_id','chapters.id')
//            ->leftJoin('completed_assignments', function($join)
//            {
//                $join->on('completed_assignments.homework_id', 'homeworks.id');
//                $join->on('completed_assignments.user_id',(int)Auth::user()->id);
//            })
//            ->leftJoin('completed_assignments','completed_assignments.homework_id','homeworks.id')
//            ->leftJoin('completed_assignments','completed_assignments.homework_id',Auth::user()->id)
//            ->where('assignments.classroom_id',Auth::user()->classroom_id)
//            ->whereRaw('completed_assignments.user_id = '.Auth::user()->id.' OR completed_assignments.user_id = null')
//            ->where('completed_assignments.user_id',Auth::user()->id)
//            ->orWhere('completed_assignments.user_id',null)
//            ->get()->toArray();
//        foreach ($all_assignments as $key => $assignment){
//            if($assignment['user_id'] != null && $assignment['user_id'] != Auth::user()->id){
//                unset($all_assignments[$key]);
//            }
//        }
            $all_assignments = DB::select('SELECT h.title,c.name,end_date,h.id,ca.user_id FROM assignments a JOIN homeworks h ON a.homework_id = h.id JOIN chapters c ON h.chapter_id = c.id LEFT JOIN completed_assignments ca ON ca.homework_id = h.id AND ca.user_id = '.Auth::user()->id.' WHERE classroom_id = '.Auth::user()->classroom_id);
//        dd($all_assignments);
        return view('assignments_history',compact('all_assignments'));
        }
        else return back()->withErrors(['not_yet' => 'Please select a classroom first.']);
    }

    public function updateUserData()
    {

        $name = $_POST['name'];
        $email = $_POST['email'];
        $classroom_id = $_POST['classroom_id'];
        $user = json_decode($_POST['user']);
//        dd($_POST,isset($classroom_id)?$classroom_id:($user->classroom_id?$user->classroom_id:null));
        if(isset($_POST['password']) && $_POST['password'] != '') {
//            dd($_POST);
            if($_POST['password'] == $_POST['password_confirmation']) {
                User::where('id',$user->id)->update([
                    'password' => Hash::make($_POST['password'])
                ]);
            } else {
                return back()->withErrors(['not_yet' => "Password don't match."]);
            }
        }
        try {
            User::where('id',$user->id)->update([
                'name' => isset($name)?$name:$user->name,
                'email' => isset($email)?$email:$user->email,
                'classroom_id' => isset($classroom_id)?$classroom_id:($user->classroom_id?$user->classroom_id:null)
            ]);
        } catch (\Throwable $th) {
            dd($th);
        }
        return Redirect('/home')->withErrors(['not_yet' => "Account updated successfully!"]);
    }

    public function assignments()
    {
        $data = Homework::select(
            'json'
        )->where('classroom_id',Auth::user()->crassroom_id+1)->get();
        $homeworks = [];
        foreach($data as $homework){
            array_push($homeworks,json_decode($homework['json'],true));
        }
        return view('assignments',compact('homeworks'));
    }

    public function projects()
    {
        return view('projects');
    }

    public function achivements()
    {
        $achivements = Achivement::get();
        return view('achivements',compact('achivements'));
    }

    public function chapters()
    {
        $chapters = Chapter::get();
        return view('chapters',compact('chapters'));
    }

    public function homework($homework_id)
    {
        return view('homework');
    }


    public function project($project_id)
    {
        return view('project');
    }

    public function lessons($chapter_id)
    {
        $lessons = Lesson::select(
            'id',
            'name',
            'description'
        )->where('chapter_id',$chapter_id)->get();
        return view('lessons',compact('lessons'));
    }

    public function lesson($lesson_id)
    {
        $lesson = Lesson::where('id',$lesson_id)->get()[0];
        return view('lesson',compact('lesson'));
    }

    public function index()
    {
        $users = User::select('users.id','name','email','classroom_id');
        $users = $users->leftJoin('classrooms','users.classroom_id','classrooms.id');
        if(isset($_GET['classroom_number'])){
            $users = $users->where('classrooms.classroom',$_GET['classroom_number']);
        }
        if(isset($_GET['classroom_letter'])){
            $users = $users->where('classrooms.letter',$_GET['classroom_letter']);
        }
        $users = $users->where('is_admin',0)->get()->toArray();
        $classrooms = Classroom::get();

        return view('admin.users',compact('users','classrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('admin.users_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);

        if($validator->fails()){
            return $validator->errors();
        }

        $input = $request->only(['name','email']);
        $input['password'] = Hash::make('kimobe_user');

        $user = User::create($input);

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();
        return view('admin.users_edit',compact('user'));
    }

    public function reset_password($id)
    {
        try {
            $user = User::where('id',$id)->update([
                'password' => Hash::make('kimobe_user')
            ]);
            $new_user = User::where('id',$id)->first();
            return "Password reset successfully for email: ".$new_user['email'];
        } catch (\Throwable $err) {
            return "An error occurred : ".$err->getMessage();
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            User::where('id',$id)->update($request->only(['name','email']));
        } catch (\Throwable $err) {
            Log::info($err->getMessage());
        }
        return redirect('/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            User::where('id',$id)->delete();
        } catch (\Throwable $err) {
            Log::info($err->getMessage());
        }
        return redirect('/users');
    }
}
