<?php

namespace App\Http\Controllers;

use App\Assignment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    public function getAssignmentsByClassroomId(){

        $id = $_POST['id'];

        $assignments = Assignment::select([
            'homeworks.id',
            'homeworks.title'
        ])
            ->where('classroom_id',$id)
            ->leftJoin('homeworks','assignments.homework_id','homeworks.id')
            ->get();

        $result = "<div>";
        foreach ($assignments as $assignment){
            $result .= "<button class='dropdown-item' onclick='selectHomework({$id},{$assignment['id']})'>{$assignment['title']}</button>";
        }
        $result .= "</div>";

        return $result;
    }

    public function getAssignmentsList(){
        $classroom_id = $_POST['classroom_id'];
        $homework_id = $_POST['homework_id'];
//        $classroom_id = 1;
//        $homework_id = 1;

        // Check if assignment was ever set to selected classroom
        $check = Assignment::where('classroom_id',$classroom_id)->where('homework_id',$homework_id)->get();

        if(!isset($check[0])){
            return "<h4>This homework was never assigned to this classroom</h4>";
        }
        else{
            $users = DB::select('SELECT * FROM users u
            LEFT JOIN completed_assignments ca ON ca.user_id = u.id AND homework_id = '.$homework_id.'
            WHERE u.classroom_id = '.$classroom_id);
            $results = "<table class='table w-100'>
                            <thead class='thead-dark' style='border-top-left-radius: 13px;border-top: none;'>
                                <tr>
                                    <th scope='col' style='border-top-left-radius: 13px;border-top: none;'>Name</th>
                                    <th>Status</th>
                                    <th scope='col'>Percent</th>
                                    <th scope='col' style='border-top-right-radius: 13px;border-top: none;'>Actions</th>
                                </tr>
                            </thead>
                            <tbody>";
            try{
                foreach ($users as $key => $data){
                    if($data->is_admin != -1){
                        $results .= "<tr>
                                    <td>{$data->name}</td>";
                        if($data->final != null){
                            $final = json_decode($data->final,true);
                            $final = round($final['total'],2);
                            $results .= "<td><button class='btn btn-success disabled' disabled style='min-width: 138px;'>Completed !</button></td>";
                            $results .= "<td>{$final}%</td>";
                            $results .= "<td><a class='btn btn-primary' target='_blank' href='/admin/homework/".$data->user_id."/".$homework_id."'>View</a></td>";
                            $results .= "</tr>";
                        }
                        else{
                            $results .= "<td><button class='btn btn-danger disabled' disabled style='min-width: 138px;'>Not Completed</button></td>";
                            $results .= "<td>0%</td>";
                            $results .= "<td></td>";
                            $results .= "</tr>";
                        }
                    }
                }
                $results .= "</tbody></table>";
                return $results;
            }catch (\Throwable $err){
                dd($err->getMessage());
            }

//            dd($users);
        }

    }
}
