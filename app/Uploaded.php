<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uploaded extends Model
{
    protected $table = 'uploaded_projects';
    protected $fillable = ['project_id','user_id','file_name','path'];

}
