<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Completed extends Model
{
    protected $table = 'completed_assignments';
    protected $fillable = ['homework_id','user_id','final','json','essay_corrected'];
}
