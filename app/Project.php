<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['end_date','title','chapter_id','json','classroom_id'];
}
