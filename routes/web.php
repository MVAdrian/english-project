<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::middleware(['admin'])->group(function () {

//    Route::resource('/users','UserCrudController');
//    Route::resource('/lessons_admin','LessonsCrudController');
    Route::get('/chapters_admin','ChaptersCrudController@index')->name('chapters_admin');
    Route::get('/chapters_admin/{id}','ChaptersCrudController@show');
    Route::post('/chapters_admin/{id}','ChaptersCrudController@update');
    Route::post('/chapters_admin/delete/{id}','ChaptersCrudController@destroy');

    Route::get('/lessons_admin','LessonsCrudController@index')->name('lessons_admin');
    Route::get('/lessons_admin/{id}','LessonsCrudController@show');
    Route::post('/lessons_admin/{id}','LessonsCrudController@update')->name('lessons_admin.update');
    Route::post('/lessons_admin/delete/{id}','LessonsCrudController@destroy');

    Route::get('/classrooms_admin','ClassroomCrudController@index')->name('classrooms_admin');
    Route::get('/classrooms_admin/add','ClassroomCrudController@create');
    Route::post('/classrooms_admin/add','ClassroomCrudController@store')->name('classrooms_admin.create');
    Route::get('/classrooms_admin/{id}','ClassroomCrudController@show');
    Route::post('/classrooms_admin/{id}','ClassroomCrudController@update')->name('classrooms_admin.update');
    Route::post('/classrooms_admin/delete/{id}','ClassroomCrudController@destroy');

    Route::get('/homework_admin','HomeworkCrudController@index')->name('homework_admin');
    Route::post('/homework_admin/delete/{id}','HomeworkCrudController@destroy')->name('homework_admin.delete');

    Route::get('/secret_key','AdminCrudController@secret')->name('secret_key');
    Route::post('/secret_key/edit','AdminCrudController@edit_secret')->name('secret_key_edit');

    Route::get('/users','UserCrudController@index')->name('user_listing');
    Route::get('/users/add','UserCrudController@create');
    Route::post('/users/add','UserCrudController@store');
    Route::get('/users/{id}','UserCrudController@show');
    Route::get('/users/reset/{id}','UserCrudController@reset_password');
    Route::post('/users/{id}','UserCrudController@update');
    Route::post('/users/delete/{id}','UserCrudController@destroy')->name('user_delete');

    Route::get('/admin','AdminCrudController@show_admin')->name('admin');
    Route::get('/admin/homework/{user_id}/{homework_id}','AdminCrudController@view_homework');

    Route::get('/create_homework','ProjectController@create_homework')->name('create_homework');
    Route::post('/create_homework','ProjectController@assign_homework')->name('assign_homework');

    Route::get('/assign_homework','ProjectController@preview_homework')->name('preview_homework');
    Route::post('/assign_homework','ProjectController@set_homework')->name('set_homework');

    Route::get('/create_project','ProjectController@create_project')->name('create_project');
    Route::post('/assign_project','ProjectController@assign_project')->name('assign_project');


    Route::get('/create_chapter','AdminCrudController@create_chapter')->name('create_chapter');
    Route::post('/assign_chapter','AdminCrudController@assign_chapter')->name('assign_chapter');

    Route::get('/create_lesson','AdminCrudController@create_lesson')->name('create_lesson');
    Route::post('/assign_lesson','AdminCrudController@assign_lesson')->name('assign_lesson');

    Route::get('/correct_assignments','AdminCrudController@correct_assignments')->name('correct_assignments');

    //AJAX
    Route::post('/getAssignmentsByClassroomId','AjaxController@getAssignmentsByClassroomId');
    Route::post('/getAssignmentsList','AjaxController@getAssignmentsList');
    //END AJAX

    Route::get('/test',function () {
        phpinfo();
    });

});

Route::get('/home', 'ProjectController@index')->name('home');
Route::post('/updateUserData','UserCrudController@updateUserData')->name('updateUserData');
Route::get('/assignments', 'UserCrudController@assignments')->name('assignments');
Route::get('/assignments_history','UserCrudController@assignments_history')->name('assignments_history');
Route::get('/projects', 'UserCrudController@projects')->name('projects');
Route::get('/chapters', 'UserCrudController@chapters')->name('chapters');
Route::get('/achivements', 'UserCrudController@achivements')->name('achivements');


Route::get('/homework/{homework_id}', 'UserCrudController@homework')->name('homework');
Route::get('/project/{project_id}', 'UserCrudController@project')->name('project');
Route::get('/lessons/{chapter_id}', 'UserCrudController@lessons')->name('lessons');
Route::get('/lesson/{lesson_id}', 'UserCrudController@lesson')->name('lesson');

Route::post('/return_homeworks','ProjectController@return_homeworks');

Route::get('/complete_homework','ProjectController@complete_homework')->name('complete_homework');
Route::post('/correct_homework','ProjectController@correct_homework')->name('correct_homework');
Route::post('/update_grades/{ca_id}','ProjectController@update_grades')->name('update_grades');
Route::get('/complete_project','ProjectController@complete_project')->name('complete_project');
Route::post('/upload_project','ProjectController@upload_project')->name('upload_project');
Route::post('/download_project','ProjectController@download_project')->name('download_project');


Route::get('/contact','ProjectController@contact')->name('contact');
Route::post('/contact_send','ProjectController@contact_send')->name('contact_send');

